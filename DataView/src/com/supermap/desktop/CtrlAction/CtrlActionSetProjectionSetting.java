package com.supermap.desktop.CtrlAction;

import java.text.MessageFormat;

import com.supermap.data.Dataset;
import com.supermap.data.DatasetType;
import com.supermap.data.Datasource;
import com.supermap.data.PrjCoordSys;
import com.supermap.desktop.Application;
import com.supermap.desktop.Interface.IBaseItem;
import com.supermap.desktop.Interface.IForm;
import com.supermap.desktop.dataview.DataViewProperties;
import com.supermap.desktop.implement.CtrlAction;
import com.supermap.desktop.ui.UICommonToolkit;
import com.supermap.desktop.ui.controls.DialogResult;
import com.supermap.desktop.ui.controls.prjcoordsys.JDialogPrjCoordSysSettings;

/**
 * 投影设置
 * 
 * @author XiaJT
 *
 */
public class CtrlActionSetProjectionSetting extends CtrlAction {

	public CtrlActionSetProjectionSetting(IBaseItem caller, IForm formClass) {
		super(caller, formClass);
	}

	@Override
	public void run() {
		Dataset[] datasets = Application.getActiveApplication().getActiveDatasets();
		Datasource[] datasources = Application.getActiveApplication().getActiveDatasources();
		PrjCoordSys prjCoordSys = null;
		// 数据集
		if (datasets.length > 0) {
			for (Dataset dataset : datasets) {
				if (dataset.getType() != DatasetType.TABULAR) {
					prjCoordSys = dataset.getPrjCoordSys();
					break;
				}
			}
		} else {
			// 数据源
			prjCoordSys = datasources[0].getPrjCoordSys();
		}

		JDialogPrjCoordSysSettings dialogPrjCoordSysSettings = new JDialogPrjCoordSysSettings();
		if (prjCoordSys != null) {
			dialogPrjCoordSysSettings.setPrjCoordSys(prjCoordSys);
		}

		if (dialogPrjCoordSysSettings.showDialog() == DialogResult.OK) {
			// 修改
			PrjCoordSys newPrjCoordSys = dialogPrjCoordSysSettings.getPrjCoordSys();
			if (datasets.length > 0) {
				for (Dataset dataset : datasets) {
					if (dataset.getType() != DatasetType.TABULAR) {
						dataset.setPrjCoordSys(newPrjCoordSys);
						Application.getActiveApplication().getOutput().output((MessageFormat
								.format(DataViewProperties.getString("String_DatasetPrjCoordSysSuccessful"), dataset.getName(), newPrjCoordSys.getName())));
					}
				}
			} else {
				for (Datasource datasource : datasources) {
					datasource.setPrjCoordSys(newPrjCoordSys);
					Application.getActiveApplication().getOutput().output((MessageFormat
							.format(DataViewProperties.getString("String_DatasourcePrjCoordSysSuccessful"), datasource.getAlias(), newPrjCoordSys.getName())));
				}
			}
		}
	}

	@Override
	public boolean enable() {
		Dataset[] datasets = Application.getActiveApplication().getActiveDatasets();
		Datasource[] datasources = Application.getActiveApplication().getActiveDatasources();
		// 选中数据集且存在不为属性表的数据集
		if (datasets.length > 0) {
			for (Dataset dataset : datasets) {
				if (DatasetType.TABULAR != dataset.getType()) {
					return true;
				}
			}
			return false;
		}
		// 选中数据源且不含只读数据源
		if (datasources.length > 0) {
			for (Datasource datasource : datasources) {
				if (datasource.isReadOnly()) {
					return false;
				}
			}
			return true;
		}
		return false;
	}
}
