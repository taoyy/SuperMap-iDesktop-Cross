package com.supermap.desktop.dataview.propertycontrols;

import java.text.MessageFormat;
import java.util.concurrent.CancellationException;

import com.supermap.data.CoordSysTransMethod;
import com.supermap.data.CoordSysTransParameter;
import com.supermap.data.CoordSysTranslator;
import com.supermap.data.Datasource;
import com.supermap.data.PrjCoordSys;
import com.supermap.data.SteppedEvent;
import com.supermap.data.SteppedListener;
import com.supermap.desktop.Application;
import com.supermap.desktop.dataview.DataViewProperties;
import com.supermap.desktop.progress.Interface.UpdateProgressCallable;
import com.supermap.desktop.ui.UICommonToolkit;
import com.supermap.desktop.ui.controls.progress.FormProgress;
import com.supermap.desktop.utilties.CursorUtilties;

public class DatasourcePrjCoordSysHandle extends PrjCoordSysHandle {
	private Datasource datasource;

	public DatasourcePrjCoordSysHandle(Datasource datasource) {
		super(datasource.getPrjCoordSys());
		this.datasource = datasource;
	}

	@Override
	public void change(PrjCoordSys targetPrj) {
		CursorUtilties.setWaitCursor();
		try {
			this.prj = targetPrj;
			this.datasource.setPrjCoordSys(this.prj);

			if (isApplyToDatasets()) {
				for (int i = 0; i < this.datasource.getDatasets().getCount(); i++) {
					this.datasource.getDatasets().get(i).setPrjCoordSys(this.prj);
				}
			}
		} catch (Exception e) {
			Application.getActiveApplication().getOutput().output(e);
		} finally {
			CursorUtilties.setDefaultCursor();
		}
	}

	@Override
	public void convert(CoordSysTransMethod method, CoordSysTransParameter parameter, PrjCoordSys targetPrj) {
		FormProgress formProgress = new FormProgress();
		formProgress.doWork(new ConvertProgressCallable(this.datasource, method, parameter, targetPrj));
	}

	private boolean isApplyToDatasets() {
		return UICommonToolkit.showConfirmDialog(MessageFormat.format(DataViewProperties.getString("String_ApplyPrjCoordSys"), this.datasource.getAlias())) == 0;
	}

	private class ConvertProgressCallable extends UpdateProgressCallable {

		private CoordSysTransMethod method;
		private CoordSysTransParameter parameter;
		private PrjCoordSys targetPrj;
		private Datasource datasource;

		private SteppedListener steppedListener = new SteppedListener() {

			@Override
			public void stepped(SteppedEvent arg0) {
				try {
					updateProgress(arg0.getPercent(), String.valueOf(arg0.getRemainTime()), arg0.getMessage());
				} catch (CancellationException e) {
					arg0.setCancel(true);
				}
			}
		};

		public ConvertProgressCallable(Datasource datasource, CoordSysTransMethod method, CoordSysTransParameter parameter, PrjCoordSys targetPrj) {
			this.datasource = datasource;
			this.method = method;
			this.parameter = parameter;
			this.targetPrj = targetPrj;

			if (this.datasource != null) {
				this.datasource.addSteppedListener(this.steppedListener);
			}
		}

		@Override
		public Boolean call() throws Exception {
			boolean result = true;

			try {
				if (this.datasource == null) {
					return false;
				}

				Application.getActiveApplication().getOutput()
						.output(MessageFormat.format(DataViewProperties.getString("String_BeginTrans_Datasource"), this.datasource.getAlias()));
				result = CoordSysTranslator.convert(this.datasource, this.targetPrj, this.parameter, this.method);

				if (result) {
					Application.getActiveApplication().getOutput()
							.output(MessageFormat.format(DataViewProperties.getString("String_CoordSysTrans_Success"), this.datasource.getAlias()));
				} else {
					Application.getActiveApplication().getOutput()
							.output(MessageFormat.format(DataViewProperties.getString("String_CoordSysTrans_Faild"), this.datasource.getAlias()));
				}
			} catch (Exception e) {
				result = false;
				Application.getActiveApplication().getOutput().output(e);
			} finally {
				if (this.datasource != null) {
					DatasourcePrjCoordSysHandle.this.prj = this.datasource.getPrjCoordSys();
					this.datasource.removeSteppedListener(this.steppedListener);
				}

				if (this.parameter != null) {
					this.parameter.dispose();
				}
			}
			return result;
		}
	}
}
