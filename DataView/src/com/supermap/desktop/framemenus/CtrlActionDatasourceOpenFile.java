package com.supermap.desktop.framemenus;

import java.awt.Component;
import java.io.File;
import java.text.MessageFormat;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import com.supermap.data.Datasource;
import com.supermap.data.DatasourceConnectionInfo;
import com.supermap.data.Datasources;
import com.supermap.data.EngineType;
import com.supermap.desktop.Application;
import com.supermap.desktop.CommonToolkit;
import com.supermap.desktop.Interface.IBaseItem;
import com.supermap.desktop.Interface.IForm;
import com.supermap.desktop.dataview.DataViewProperties;
import com.supermap.desktop.implement.CtrlAction;
import com.supermap.desktop.properties.CommonProperties;
import com.supermap.desktop.properties.CoreProperties;
import com.supermap.desktop.ui.UICommonToolkit;
import com.supermap.desktop.ui.controls.SmFileChoose;

public class CtrlActionDatasourceOpenFile extends CtrlAction {

	private Set<String> pluginSet = new HashSet<String>();
	private Set<String> vectorSet = new HashSet<String>();
	private String[] pluginString = new String[] { "sit", "bmp", "jpg", "jpeg", "png", "tif", "tiff", "gif" };
	private String[] vectorString = new String[] { "shp", "mif", "dxf", "dwg", "kml", "kmz", "wal", "wan" };

	public CtrlActionDatasourceOpenFile(IBaseItem caller, IForm formClass) {
		super(caller, formClass);
	}

	private Datasource openImagePluginDatasource(File file, EngineType engineType) {
		Datasource resultDatasource = null;
		try {
			Datasources datasources = Application.getActiveApplication().getWorkspace().getDatasources();
			DatasourceConnectionInfo info = new DatasourceConnectionInfo();
			String alias = file.getName();
			int dot = alias.lastIndexOf('.');
			if ((dot > -1) && (dot < (alias.length()))) {
				alias = alias.substring(0, dot);
			}
			Set<String> datasourceAlias = new HashSet<String>();
			for (int i = 0; i < datasources.getCount(); i++) {
				datasourceAlias.add(datasources.get(i).getAlias());
			}
			// 没有同名数据源时才打开
			if (!datasourceAlias.contains(alias)) {
				// 数据源重命名
				alias = CommonToolkit.DatasourceWrap.getAvailableDatasourceAlias(alias, 0);
				info.setAlias(alias);
				info.setServer(file.getPath());
				info.setEngineType(engineType);
				info.setReadOnly(!file.canWrite() || info.getEngineType() == engineType);

				resultDatasource = Application.getActiveApplication().getWorkspace().getDatasources().open(info);
				if (null != resultDatasource) {
					UICommonToolkit.refreshSelectedDatasourceNode(resultDatasource.getAlias());
				}
			}
		} catch (Exception e) {
			Application.getActiveApplication().getOutput().output(e);
		}
		return resultDatasource;
	}

	private void initSetInfo() {
		for (int i = 0; i < pluginString.length; i++) {
			pluginSet.add(pluginString[i]);
			pluginSet.add(pluginString[i].toUpperCase());
		}
		for (int i = 0; i < pluginString.length; i++) {
			vectorSet.add(vectorString[i]);
			vectorSet.add(vectorString[i].toUpperCase());
		}
	}

	@Override
	public void run() {
		try {
			/**
			 * 需要选中的数据源名称
			 */
			initSetInfo();
			if (!SmFileChoose.isModuleExist("DatasourceOpenFile")) {
				String fileFilterAll = SmFileChoose.createFileFilter(DataViewProperties.getString("String_FileFilters_SupportType"),
						new String[] { "sit", "bmp", "jpg", "jpeg", "png", "tif", "tiff", "gif", "shp", "mif", "dxf", "dwg", "kml", "kmz", "wal", "wan", "udb" });
				String fileFilterUdb = SmFileChoose.createFileFilter(DataViewProperties.getString("String_FileFilters_Datasourse"), "udb");
				String fileFilterImagePlugin = SmFileChoose.createFileFilter(DataViewProperties.getString("String_FileFilters_ImagePlugins"), pluginString);
				String fileFilterVector = SmFileChoose.createFileFilter(DataViewProperties.getString("String_FileFilters_Vector"), vectorString);
				String fileFiltersString = SmFileChoose.bulidFileFilters(fileFilterAll, fileFilterUdb, fileFilterImagePlugin, fileFilterVector);
				SmFileChoose.addNewNode(fileFiltersString, CommonProperties.getString("String_DefaultFilePath"),
						DataViewProperties.getString("String_Title_DatasoursesOpenFile"), "DatasourceOpenFile", "OpenMany");
			}

			SmFileChoose openFileDlg = new SmFileChoose("DatasourceOpenFile");
			Component parent = (Component) Application.getActiveApplication().getMainFrame();
			if (openFileDlg.showDefaultDialog() == JFileChooser.APPROVE_OPTION) {
				String message = "";
				int failedCount = 0;

				boolean isReadOnlyOpen = true;
				boolean isAllDatasourceExcuteThis = false;
				File[] files = openFileDlg.getSelectedFiles();
				for (int i = 0; i < files.length; i++) {
					String filePath = files[i].getPath();
					boolean isReadOnly = false;

					File uddFile = null;
					Datasource datasource = null;
					String openFilePath = openFileDlg.getFilePath();
					String fileType = openFilePath.substring(openFilePath.lastIndexOf(".") + 1, openFilePath.length());
					if (filePath.toLowerCase().endsWith(".udb")) {
						String uddFilePath = filePath.substring(0, filePath.lastIndexOf(".")) + ".udd";
						uddFile = new File(uddFilePath);
					} else if (pluginSet.contains(fileType)) {
						datasource = openImagePluginDatasource(files[i], EngineType.IMAGEPLUGINS);
					} else if (vectorSet.contains(fileType)) {
						datasource = openImagePluginDatasource(files[i], EngineType.VECTORFILE);
					}
					// TODO 文件只读时提示
					if (null != uddFile && uddFile.exists()) {
						if (!isReadOnly && (!files[i].canWrite() || !uddFile.canWrite())) {
							if (!isAllDatasourceExcuteThis) {
								message = MessageFormat.format(CoreProperties.getString("String_OpenReadOnlyDatasourceWarning"), filePath);

								int dialogResult = UICommonToolkit.showConfirmDialog(message);
								if (dialogResult == JOptionPane.YES_OPTION) {
									isReadOnly = true;
								} else {
									isReadOnlyOpen = false;
								}
								isAllDatasourceExcuteThis = true;
							} else {
								isReadOnly = true;
							}

							if (isReadOnlyOpen) {
								datasource = CommonToolkit.DatasourceWrap.openFileDatasource(filePath, "", isReadOnly, true);
							} else {
								message = String.format(CoreProperties.getString("String_OpenDatasourceFailed"), filePath);
								Application.getActiveApplication().getOutput().output(message);
								failedCount++;
								continue;
							}
						} else {
							datasource = CommonToolkit.DatasourceWrap.openFileDatasource(filePath, "", isReadOnly, true);
							if (null != datasource) {
								// 设置数据源选中
								UICommonToolkit.refreshSelectedDatasourceNode(datasource.getAlias());
							}
						}
					}

					if (datasource == null) {
						// 如果是密码错误就提示让用户输入密码再打开一次，如果还失败则不再提示了。
						boolean isCancel = false;
						while (datasource == null && CommonToolkit.DatasourceWrap.isPasswordWrong() && !isCancel) {
							String info = String.format(CoreProperties.getString("String_InputDatasourcePassword"), files[i].getName());

							String passWord = JOptionPane.showInputDialog(parent, info, "");
							if (passWord == null) {
								isCancel = true;
							} else {
								datasource = CommonToolkit.DatasourceWrap.openFileDatasource(filePath, passWord, isReadOnly, false);
							}
						}
					}

					if (datasource == null) {
						failedCount++;
						message = String.format(CoreProperties.getString("String_OpenDatasourceFailed"), filePath);
						Application.getActiveApplication().getOutput().output(message);
					}
				}

				if (openFileDlg.getSelectedFiles().length > 1) {
					message = String.format(CoreProperties.getString("String_OpenDatasourceResultMsg"), openFileDlg.getSelectedFiles().length,
							openFileDlg.getSelectedFiles().length - failedCount, failedCount);
					Application.getActiveApplication().getOutput().output(message);
				}
			}

		} catch (Exception ex) {
			Application.getActiveApplication().getOutput().output(ex);
		}
	}

	@Override
	public boolean enable() {
		return true;
	}
}
