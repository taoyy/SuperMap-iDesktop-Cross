package com.supermap.desktop.mapview.map.propertycontrols;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.plaf.metal.MetalBorders;

import com.supermap.desktop.Application;
import com.supermap.desktop.mapview.MapViewProperties;
import com.supermap.desktop.utilties.PrjCoordSysUtilties;
import com.supermap.mapping.Map;

public class MapPrjCoordSysControl extends AbstractPropertyControl {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JCheckBox checkBoxIsDynamicProjection;
	private JButton buttonProjectionSetting;
	private JLabel labelCoordName;
	private JTextField textFieldCoordName;
	private JLabel labelCoordUnit;
	private JTextField textFieldCoordUnit;
	private JTextArea textAreaCoordInfo;

	private boolean isDynamicProjection = false;

	private ItemListener checkboxItemListener = new ItemListener() {

		@Override
		public void itemStateChanged(ItemEvent e) {
			checkBoxIsDynamicProjectionCheckedChange();
		}
	};

	/**
	 * Create the panel.
	 */
	public MapPrjCoordSysControl() {
		super(MapViewProperties.getString("String_TabPage_ProjectionSetting"));
	}

	@Override
	public void apply() {
		getMap().setDynamicProjection(isDynamicProjection);
	}

	@Override
	protected void initializeComponents() {
		this.checkBoxIsDynamicProjection = new JCheckBox("DynamicProjection");
		this.buttonProjectionSetting = new JButton("ProjectionSetting");
		this.labelCoordName = new JLabel("CoordName:");
		this.textFieldCoordName = new JTextField();
		this.textFieldCoordName.setEditable(false);
		this.labelCoordUnit = new JLabel("CoordUnit:");
		this.textFieldCoordUnit = new JTextField();
		this.textFieldCoordUnit.setEditable(false);
		this.textAreaCoordInfo = new JTextArea();
		this.textAreaCoordInfo.setEditable(false);
		this.textAreaCoordInfo.setBorder(MetalBorders.getTextFieldBorder());

		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setAutoCreateContainerGaps(true);
		groupLayout.setAutoCreateGaps(true);
		this.setLayout(groupLayout);

		// @formatter:off
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
						.addComponent(this.checkBoxIsDynamicProjection)
						.addComponent(this.buttonProjectionSetting, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addComponent(this.labelCoordName)
				.addComponent(this.textFieldCoordName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
				.addComponent(this.labelCoordUnit)
				.addComponent(this.textFieldCoordUnit, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
				.addComponent(this.textAreaCoordInfo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
		
		groupLayout.setVerticalGroup(groupLayout.createSequentialGroup()
				.addGroup(groupLayout.createParallelGroup(Alignment.CENTER)
						.addComponent(this.checkBoxIsDynamicProjection)
						.addComponent(this.buttonProjectionSetting, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
				.addComponent(this.labelCoordName)
				.addComponent(this.textFieldCoordName, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				.addComponent(this.labelCoordUnit)
				.addComponent(this.textFieldCoordUnit, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				.addComponent(this.textAreaCoordInfo, 100, 200, Short.MAX_VALUE));
		// @formatter:on
	}

	@Override
	protected void initializeResources() {
		this.checkBoxIsDynamicProjection.setText(MapViewProperties.getString("String_CheckBox_DynamicProjection"));
		this.buttonProjectionSetting.setText(MapViewProperties.getString("String_Button_ProjectionSetting"));
		this.labelCoordName.setText(MapViewProperties.getString("String_Label_PrjCoordSysName"));
		this.labelCoordUnit.setText(MapViewProperties.getString("String_Label_PrjCoordSysUnit"));
	}

	@Override
	protected void initializePropertyValues(Map map) {
		this.isDynamicProjection = map.isDynamicProjection();
	}

	@Override
	protected void registerEvents() {
		this.checkBoxIsDynamicProjection.addItemListener(this.checkboxItemListener);
	}

	@Override
	protected void unregisterEvents() {
		this.checkBoxIsDynamicProjection.removeItemListener(this.checkboxItemListener);
	}

	@Override
	protected void fillComponents() {
		this.checkBoxIsDynamicProjection.setSelected(this.isDynamicProjection);
		this.textFieldCoordName.setText(getMap().getPrjCoordSys().getName());
		this.textFieldCoordUnit.setText(getMap().getPrjCoordSys().getCoordUnit().toString());
		this.textAreaCoordInfo.setText(PrjCoordSysUtilties.getDescription(getMap().getPrjCoordSys()));
	}

	@Override
	protected void setComponentsEnabled() {
		this.buttonProjectionSetting.setEnabled(this.isDynamicProjection);
		this.textFieldCoordName.setEnabled(false);
		this.textFieldCoordUnit.setEnabled(false);
		this.textAreaCoordInfo.setEnabled(false);
	}

	@Override
	protected boolean verifyChange() {
		return this.isDynamicProjection != getMap().isDynamicProjection();
	}

	private void checkBoxIsDynamicProjectionCheckedChange() {
		try {
			this.isDynamicProjection = this.checkBoxIsDynamicProjection.isSelected();
			verify();
		} catch (Exception e2) {
			Application.getActiveApplication().getOutput().output(e2);
		}
	}
}
