package com.supermap.desktop.mapview.layer.propertymodel;

import com.supermap.data.Dataset;
import com.supermap.desktop.Application;
import com.supermap.mapping.Layer;
import com.supermap.mapping.LayerSettingVector;
import com.supermap.mapping.Map;

public class LayerRelocateDatasetPropertyModel extends LayerPropertyModel {

	public static final String DATASET = "dataset";

	private Dataset dataset;

	public LayerRelocateDatasetPropertyModel() {
		// TODO
	}

	public LayerRelocateDatasetPropertyModel(Layer[] layers, Map map) {
		super(layers, map);
		initializeProperties(layers, map);
	}

	public Dataset getDataset() {
		return this.dataset;
	}

	public void setDataset(Dataset dataset) {
		this.dataset = dataset;
	}

	private void initializeProperties(Layer[] layers, Map map) {
		this.dataset = layers[0].getDataset();
		this.propertyEnabled.put(DATASET, true);
		checkPropertyEnabled();

		if (layers != null && map != null && layers.length > 0) {
			for (Layer layer : layers) {
				if (layer == null) {
					break;
				}

				this.dataset = ComplexPropertyUtilties.union(this.dataset, layer.getDataset());
			}
		}
	}

	@Override
	protected void apply(Layer layer) {
		layer.setDataset(this.dataset);
	}

	@Override
	public void setProperties(LayerPropertyModel model) {
		this.dataset = ((LayerRelocateDatasetPropertyModel) model).getDataset();
	}

	/**
	 * 子类必须重写这个方法
	 * 
	 * @param model
	 * @return
	 */
	@Override
	public boolean equals(LayerPropertyModel model) {
		LayerRelocateDatasetPropertyModel basePropertyModel = (LayerRelocateDatasetPropertyModel) model;

		return super.equals(basePropertyModel) && this.dataset == basePropertyModel.getDataset();
	}

	private void checkPropertyEnabled() {
		try {
			if (getLayers() != null && getMap() != null && getLayers().length > 1) {
				checkPropertyEnabled(DATASET, false);
			}
		} catch (Exception e) {
			Application.getActiveApplication().getOutput().output(e);
		}
	}
}
