package com.supermap.desktop.CtrlAction.Theme;

import java.util.ArrayList;

import javax.swing.JFrame;

import com.supermap.data.Dataset;
import com.supermap.data.DatasetType;
import com.supermap.desktop.Application;
import com.supermap.desktop.Interface.IBaseItem;
import com.supermap.desktop.Interface.IForm;
import com.supermap.desktop.Interface.IFormMap;
import com.supermap.desktop.implement.CtrlAction;
import com.supermap.desktop.newtheme.ThemeGuidDialog;
import com.supermap.desktop.theme.CreateThemeContainer;
//import com.supermap.desktop.themeguid.ThemeGuidDialog;
import com.supermap.mapping.Map;

public class CtrlActionThemeGuide extends CtrlAction {
	public CtrlActionThemeGuide(IBaseItem caller, IForm formClass) {
		super(caller, formClass);
	}

	public void run() {
		JFrame frame = (JFrame) Application.getActiveApplication().getMainFrame();
//		 CreateThemeContainer container = new CreateThemeContainer(frame, true);
//		 container.setVisible(true);
		ThemeGuidDialog themeGuidDialog = new ThemeGuidDialog(frame, true);
		themeGuidDialog.setVisible(true);
	}

	public boolean enable() {
		boolean enable = false;
		ArrayList<DatasetType> enableDatasetTypes = new ArrayList<DatasetType>();
		enableDatasetTypes.add(DatasetType.LINE);
		enableDatasetTypes.add(DatasetType.LINE3D);
		enableDatasetTypes.add(DatasetType.LINEM);
		enableDatasetTypes.add(DatasetType.NETWORK);
		enableDatasetTypes.add(DatasetType.NETWORK3D);
		enableDatasetTypes.add(DatasetType.POINT);
		enableDatasetTypes.add(DatasetType.POINT3D);
		enableDatasetTypes.add(DatasetType.REGION);
		enableDatasetTypes.add(DatasetType.REGION3D);

		if (null != Application.getActiveApplication().getActiveForm()) {
			IFormMap formMap = (IFormMap) Application.getActiveApplication().getActiveForm();
			Map map = formMap.getMapControl().getMap();
			for (int i = 0; i < map.getLayers().getCount(); i++) {
				if (map.getLayers().get(i).getDataset() != null && enableDatasetTypes.contains(map.getLayers().get(i).getDataset().getType())) {
					enable = true;
				}
			}
		} else if (Application.getActiveApplication().getActiveDatasets().length > 0) {
			Dataset activeDataset = Application.getActiveApplication().getActiveDatasets()[0];
			if (enableDatasetTypes.contains(activeDataset.getType())) {
				enable = true;
			}
		}
		return enable;
	}
}
