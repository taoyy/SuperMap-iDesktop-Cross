package com.supermap.desktop.newtheme;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;

import com.supermap.data.ColorGradientType;
import com.supermap.data.Colors;
import com.supermap.data.Dataset;
import com.supermap.data.DatasetType;
import com.supermap.data.DatasetVector;
import com.supermap.data.FieldInfo;
import com.supermap.data.FieldType;
import com.supermap.data.GeoStyle;
import com.supermap.data.Resources;
import com.supermap.data.SymbolType;
import com.supermap.desktop.Application;
import com.supermap.desktop.CommonToolkit;
import com.supermap.desktop.Interface.IFormMap;
import com.supermap.desktop.mapview.MapViewProperties;
import com.supermap.desktop.ui.UICommonToolkit;
import com.supermap.desktop.ui.controls.ColorsComboBox;
import com.supermap.desktop.ui.controls.DialogResult;
import com.supermap.desktop.ui.controls.GridBagConstraintsHelper;
import com.supermap.desktop.ui.controls.InternalImageIconFactory;
import com.supermap.desktop.ui.controls.SQLExpressionDialog;
import com.supermap.desktop.ui.controls.SymbolDialog;
import com.supermap.mapping.Layer;
import com.supermap.mapping.Map;
import com.supermap.mapping.ThemeUnique;
import com.supermap.mapping.ThemeUniqueItem;
import com.supermap.ui.MapControl;

import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.DefaultComboBoxModel;

public class ThemeUniqueContainer extends JPanel {

	private static final long serialVersionUID = 1L;
	private JTabbedPane tabbedPaneInfo = new JTabbedPane(JTabbedPane.TOP);
	private JPanel panelProperty = new JPanel();
	private JPanel panelAdvance = new JPanel();
	private JLabel labelExpression = new JLabel();
	private JComboBox<String> comboBoxExpression = new JComboBox<String>();
	private JLabel labelColorStyle = new JLabel();
	private ColorsComboBox comboboxColor = new ColorsComboBox();
	private JToolBar toolBar = new JToolBar();
	private JLabel labelUnique = new JLabel();
	private JTextField textFieldUnique = new JTextField();
	private JScrollPane scollPane = new JScrollPane();
	private JTable tableUniqueInfo = new JTable();
	private JButton buttonVisble = new JButton();
	private JButton buttonGeoSytle = new JButton();
	private JButton buttonAdd = new JButton();
	private JButton buttonDelete = new JButton();
	private JButton buttonAscend = new JButton();
	private JButton buttonDescend = new JButton();
	private JButton buttonAntitone = new JButton();
	private JPanel panelOffsetSet = new JPanel();
	private JLabel labelOffsetUnity = new JLabel();
	private JComboBox<String> comboBoxOffsetUnity = new JComboBox<String>();
	private JLabel labelOffsetX = new JLabel();
	private JLabel labelOffsetXUnity = new JLabel();
	private JComboBox<String> comboBoxOffsetX = new JComboBox<String>();
	private JLabel labelOffsetY = new JLabel();
	private JLabel labelOffsetYUnity = new JLabel();
	private JComboBox<String> comboBoxOffsetY = new JComboBox<String>();
	private String[] nameStrings = { MapViewProperties.getString("String_Title_Visible"), MapViewProperties.getString("String_Title_Sytle"),
			MapViewProperties.getString("String_ThemeGraphItemManager_ClmExpression") };
	private ThemeUnique themeUnique;
	private DatasetVector datasetVector;
	private SQLExpressionDialog sqlDialog;
	private LocalComboBoxItemListener comboBoxItemListener = new LocalComboBoxItemListener();
	private LocalActionListener actionListener = new LocalActionListener();
	private Layer themeUniqueLayer;
	private Map map;
	private ArrayList<ThemeUniqueItem> deleteItems;
	private static int TABLE_COLUMN_VISIBLE = 0;
	private static int TABLE_COLUMN_GEOSTYLE=1;
	private static int TABLE_COLUMN_CAPTION=2;
	

	public ThemeUniqueContainer(DatasetVector datasetVector) {
		this.datasetVector = datasetVector;
		this.map = initCurrentTheme(datasetVector);
		initComponents();
		initResources();
		registActionListener();
	}

	/**
	 * 初始化单值专题图
	 * 
	 * @param dataset
	 * @return
	 */
	private Map initCurrentTheme(DatasetVector dataset) {
		this.themeUnique = ThemeUnique.makeDefault(dataset, "SmID", ColorGradientType.YELLOWGREEN);
		IFormMap formMap = (IFormMap) Application.getActiveApplication().getActiveForm();
		MapControl mapControl = null;
		if (null != formMap.getMapControl()) {
			mapControl = formMap.getMapControl();
			this.themeUniqueLayer = mapControl.getMap().getLayers().add(dataset, themeUnique, true);
			this.themeUnique = (ThemeUnique) themeUniqueLayer.getTheme();
			mapControl.getMap().refresh();
		}
		return mapControl.getMap();
	}

	/**
	 * 界面布局入口
	 */
	private void initComponents() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		this.setLayout(gridBagLayout);

		this.tabbedPaneInfo.add(MapViewProperties.getString("String_Theme_Property"), this.panelProperty);
		this.tabbedPaneInfo.add(MapViewProperties.getString("String_Theme_Advanced"), this.panelAdvance);
		this.add(tabbedPaneInfo, new GridBagConstraintsHelper(0, 0, 1, 1).setAnchor(GridBagConstraints.CENTER).setFill(GridBagConstraints.BOTH).setWeight(1, 1));
		initPanelProperty();
		initPanelAdvance();
		getFieldComboBox(this.comboBoxExpression);
		getFieldComboBox(this.comboBoxOffsetX);
		this.comboBoxOffsetX.addItem("0");
		this.comboBoxOffsetX.setSelectedIndex(this.comboBoxOffsetX.getItemCount() - 1);
		getFieldComboBox(this.comboBoxOffsetY);
		this.comboBoxOffsetY.addItem("0");
		this.comboBoxOffsetY.setSelectedIndex(this.comboBoxOffsetX.getItemCount() - 1);
	}

	/**
	 * 资源化
	 */
	private void initResources() {
		this.labelExpression.setText(MapViewProperties.getString("String_label_Expression"));
		this.labelColorStyle.setText(MapViewProperties.getString("String_Label_ColorScheme"));
		this.labelUnique.setText(MapViewProperties.getString("String_Label_Unique"));
		this.buttonVisble.setToolTipText(MapViewProperties.getString("String_Title_Visible"));
		this.buttonGeoSytle.setToolTipText(MapViewProperties.getString("String_Title_Sytle"));
		this.buttonAdd.setToolTipText(MapViewProperties.getString("String_Title_Add"));
		this.buttonDelete.setToolTipText(MapViewProperties.getString("String_Title_Delete"));
		this.buttonAscend.setToolTipText(MapViewProperties.getString("String_Title_Ascend"));
		this.buttonDescend.setToolTipText(MapViewProperties.getString("String_Title_Descend"));
		this.buttonAntitone.setToolTipText(MapViewProperties.getString("String_Title_Antitone"));
		this.panelOffsetSet.setBorder(new TitledBorder(null, MapViewProperties.getString("String_GroupBoxOffset"), TitledBorder.LEADING, TitledBorder.TOP,
				null, null));
		this.labelOffsetUnity.setText(MapViewProperties.getString("String_LabelOffsetUnit"));
		this.labelOffsetX.setText(MapViewProperties.getString("String_LabelOffsetX"));
		this.labelOffsetY.setText(MapViewProperties.getString("String_LabelOffsetY"));
		this.labelOffsetXUnity.setText(MapViewProperties.getString("String_Combobox_MM"));
		this.labelOffsetYUnity.setText(MapViewProperties.getString("String_Combobox_MM"));
		this.comboBoxOffsetUnity.setModel(new DefaultComboBoxModel<String>(new String[] {
				MapViewProperties.getString("String_MapBorderLineStyle_LabelDistanceUnit"), MapViewProperties.getString("String_ThemeLabelOffsetUnit_Map") }));
		this.comboBoxOffsetUnity.setEditable(true);
		this.comboBoxOffsetX.setEditable(true);
		this.comboBoxOffsetY.setEditable(true);
		this.textFieldUnique.setText(themeUnique.getItem(0).getCaption());
	}

	/**
	 * 控件注册事件
	 */
	private void registActionListener() {
		this.comboBoxExpression.addItemListener(this.comboBoxItemListener);
		this.comboBoxOffsetX.addItemListener(this.comboBoxItemListener);
		this.comboBoxOffsetY.addItemListener(this.comboBoxItemListener);
		this.comboboxColor.addItemListener(this.comboBoxItemListener);
		this.comboBoxOffsetUnity.addItemListener(this.comboBoxItemListener);
		this.buttonVisble.addActionListener(this.actionListener);
		this.buttonAdd.addActionListener(this.actionListener);
		this.buttonDelete.addActionListener(this.actionListener);
		this.buttonGeoSytle.addActionListener(this.actionListener);
		this.buttonAntitone.addActionListener(this.actionListener);
	}

	/**
	 * 销毁事件
	 */
	private void unregistActionListener() {
		this.comboBoxExpression.removeItemListener(this.comboBoxItemListener);
		this.comboBoxOffsetX.removeItemListener(this.comboBoxItemListener);
		this.comboBoxOffsetY.removeItemListener(this.comboBoxItemListener);
		this.comboboxColor.removeItemListener(this.comboBoxItemListener);
		this.comboBoxOffsetUnity.removeItemListener(this.comboBoxItemListener);
		this.buttonVisble.removeActionListener(this.actionListener);
		this.buttonAdd.removeActionListener(this.actionListener);
		this.buttonDelete.removeActionListener(this.actionListener);
		this.buttonGeoSytle.removeActionListener(this.actionListener);
		this.buttonAntitone.removeActionListener(this.actionListener);
	}

	/**
	 * 高级面板布局
	 */
	private void initPanelAdvance() {
		this.panelAdvance.setLayout(new GridBagLayout());
		//@formatter:off
		this.panelAdvance.add(this.panelOffsetSet, new GridBagConstraintsHelper(0, 0, 1, 1).setWeight(1, 1).setAnchor(GridBagConstraints.NORTH).setFill(GridBagConstraints.HORIZONTAL).setInsets(5, 10, 5, 10));
		this.panelOffsetSet.setLayout(new GridBagLayout());
		this.panelOffsetSet.add(this.labelOffsetUnity,    new GridBagConstraintsHelper(0, 0, 1, 1).setAnchor(GridBagConstraints.CENTER).setInsets(5,10,5,10).setWeight(1, 1));
		this.panelOffsetSet.add(this.comboBoxOffsetUnity, new GridBagConstraintsHelper(1, 0, 2, 1).setAnchor(GridBagConstraints.CENTER).setInsets(5,10,5,10).setWeight(1, 1).setFill(GridBagConstraints.HORIZONTAL));
		this.panelOffsetSet.add(this.labelOffsetX,        new GridBagConstraintsHelper(0, 1, 1, 1).setAnchor(GridBagConstraints.CENTER).setInsets(5,10,5,10).setWeight(1, 1));
		this.panelOffsetSet.add(this.comboBoxOffsetX,     new GridBagConstraintsHelper(1, 1, 1, 1).setAnchor(GridBagConstraints.CENTER).setInsets(5,10,5,10).setWeight(1, 1).setFill(GridBagConstraints.HORIZONTAL));
		this.panelOffsetSet.add(this.labelOffsetXUnity,   new GridBagConstraintsHelper(2, 1, 1, 1).setAnchor(GridBagConstraints.CENTER).setInsets(5,10,5,10).setWeight(1, 1));
		this.panelOffsetSet.add(this.labelOffsetY,        new GridBagConstraintsHelper(0, 2, 1, 1).setAnchor(GridBagConstraints.CENTER).setInsets(5,10,5,10).setWeight(1, 1));
		this.panelOffsetSet.add(this.comboBoxOffsetY,     new GridBagConstraintsHelper(1, 2, 1, 1).setAnchor(GridBagConstraints.CENTER).setInsets(5,10,5,10).setWeight(1, 1).setFill(GridBagConstraints.HORIZONTAL));
		this.panelOffsetSet.add(this.labelOffsetYUnity,   new GridBagConstraintsHelper(2, 2, 1, 1).setAnchor(GridBagConstraints.CENTER).setInsets(5,10,5,10).setWeight(1, 1));
		//@formatter:on
	}

	/**
	 * 初始化工具条
	 */
	private void initToolBar() {
		this.buttonVisble.setIcon(InternalImageIconFactory.VISIBLE);
		this.buttonGeoSytle.setIcon(InternalImageIconFactory.STYLE);
		this.buttonAdd.setIcon(new ImageIcon(ThemeUnique.class.getResource("/com/supermap/desktop/coreresources/ToolBar/Image_ToolButton_AddMap.png")));
		this.buttonDelete.setIcon(new ImageIcon(ThemeUnique.class.getResource("/com/supermap/desktop/coreresources/ToolBar/Image_ToolButton_Delete.png")));
		this.buttonAntitone.setIcon(InternalImageIconFactory.Rever);
	}

	/**
	 * 属性面板布局
	 */
	private void initPanelProperty() {
		this.panelProperty.setLayout(new GridBagLayout());
		toolBar.setFloatable(false);
		this.toolBar.add(this.buttonVisble);
		this.toolBar.add(this.buttonGeoSytle);
		this.toolBar.addSeparator();
		this.toolBar.add(this.buttonAdd);
		this.toolBar.add(this.buttonDelete);
		// this.toolBar.add(this.buttonAscend);
		// this.toolBar.add(this.buttonDescend);
		// this.toolBar.addSeparator();
		this.toolBar.add(this.buttonAntitone);
		initToolBar();
		//@formatter:off
		this.panelProperty.add(this.labelExpression,    new GridBagConstraintsHelper(0, 0, 1, 1).setAnchor(GridBagConstraints.WEST).setInsets(5, 20, 5, 5).setIpad(30, 0).setWeight(1, 0));
		comboBoxExpression.setEditable(true);
		this.panelProperty.add(this.comboBoxExpression, new GridBagConstraintsHelper(1, 0, 1, 1).setAnchor(GridBagConstraints.WEST).setInsets(5, 10, 5, 5).setWeight(1, 0));
		this.panelProperty.add(this.labelColorStyle,    new GridBagConstraintsHelper(0, 1, 1, 1).setAnchor(GridBagConstraints.WEST).setInsets(5, 20, 5, 5).setIpad(40, 0).setWeight(1, 0));
		this.panelProperty.add(this.comboboxColor,      new GridBagConstraintsHelper(1, 1, 1, 1).setAnchor(GridBagConstraints.WEST).setInsets(5, 10, 5, 5).setWeight(1, 0).setIpad(25, 0));
		this.panelProperty.add(this.toolBar,            new GridBagConstraintsHelper(0, 2, 2, 1).setAnchor(GridBagConstraints.WEST).setInsets(5, 10, 5, 5).setIpad(40, 0).setWeight(1, 0));
		this.panelProperty.add(this.labelUnique,        new GridBagConstraintsHelper(0, 3, 1, 1).setAnchor(GridBagConstraints.WEST).setInsets(5, 20, 5, 5).setIpad(40, 0).setWeight(1, 0));
		this.panelProperty.add(this.textFieldUnique,    new GridBagConstraintsHelper(1, 3, 1, 1).setAnchor(GridBagConstraints.WEST).setInsets(5, 10, 5, 5).setWeight(1, 0).setIpad(200, 0));
		this.panelProperty.add(this.scollPane,          new GridBagConstraintsHelper(0, 4, 2, 1).setAnchor(GridBagConstraints.NORTH).setInsets(5, 10, 5, 5).setFill(GridBagConstraints.BOTH).setWeight(1, 3));
		getTable();
		this.scollPane.setViewportView(tableUniqueInfo);		
		//@formatter:on
	}

	/**
	 * 表格初始化
	 * 
	 * @return m_table
	 */
	private JTable getTable() {
		DefaultTableModel defaultTableModel = new DefaultTableModel(getData(), nameStrings);
		this.tableUniqueInfo = new JTable(defaultTableModel);
		this.tableUniqueInfo.getTableHeader().setReorderingAllowed(false);
		this.tableUniqueInfo.setRowSelectionAllowed(true);
		this.tableUniqueInfo.setColumnSelectionAllowed(true);
		this.tableUniqueInfo.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		this.tableUniqueInfo.setRowHeight(25);
		initTableRender();
		return this.tableUniqueInfo;
	}

	/**
	 * table渲染和颜色控件事件修改
	 */
	private void initTableRender() {
		TableColumn visibleColumn = tableUniqueInfo.getColumn(MapViewProperties.getString("String_Title_Visible"));
		TableColumn viewColumn = tableUniqueInfo.getColumn(MapViewProperties.getString("String_Title_Sytle"));
		JPanelCellEditorGeoStyle cellEditor = new JPanelCellEditorGeoStyle(datasetVector);
		JPanelRendererGeoStyle cellRenderer = new JPanelRendererGeoStyle();
		cellEditor.addPropertyChangeListener("Color",
				new PropertyChangeListener() {
					public void propertyChange(PropertyChangeEvent evt) {
						int index = tableUniqueInfo.getSelectedRow();
						GeoStyle textStyle = (GeoStyle) evt.getNewValue();
						((ThemeUnique) themeUniqueLayer.getTheme()).getItem(index).setStyle(textStyle);
						map.refresh();
						tableUniqueInfo.repaint();
					}
				});
		JPanelCellEditorVisible visibleEditor = new JPanelCellEditorVisible(JPanelCellEditorVisible.THEME_UNIQUE_TYPE);
		JPanelRenderVisible visibleRender = new JPanelRenderVisible();
		visibleEditor.addPropertyChangeListener("Visible",
				new PropertyChangeListener() {
					public void propertyChange(PropertyChangeEvent evt) {
						int index = tableUniqueInfo.getSelectedRow();
						boolean visible = (boolean) evt.getNewValue();
						((ThemeUnique) themeUniqueLayer.getTheme()).getItem(index).setVisible(visible);
						map.refresh();
						tableUniqueInfo.repaint();
					}
				});
		visibleColumn.setCellEditor(visibleEditor);
		visibleColumn.setCellRenderer(visibleRender);
		viewColumn.setCellRenderer(cellRenderer);
		viewColumn.setCellEditor(cellEditor);
		
	}

	/**
	 * 获取表格需要的数据
	 * 
	 * @return tableData
	 */
	private Object[][] getData() {
		int uniqueCount = this.themeUnique.getCount();
		if (uniqueCount == 0) {
			return new Object[0][3];
		}
		Object[][] tableData = new Object[uniqueCount][3];
		for (int i = 0; i < uniqueCount; i++) {
			ThemeUniqueItem uniqueItem = themeUnique.getItem(i);
			GeoStyle geoStyle = uniqueItem.getStyle();
			UniqueValue uniqueValue = new UniqueValue();
			DatasetType datasetType = datasetVector.getType();
			if (CommonToolkit.DatasetTypeWrap.isPoint(datasetType) || CommonToolkit.DatasetTypeWrap.isLine(datasetType)) {
				uniqueValue.setColor(geoStyle.getLineColor());
			} else if (CommonToolkit.DatasetTypeWrap.isRegion(datasetType)) {
				uniqueValue.setColor(geoStyle.getFillForeColor());
			}
			VisibleValue visibleValue = new VisibleValue(uniqueItem);
			tableData[i][TABLE_COLUMN_VISIBLE] = visibleValue;
			tableData[i][TABLE_COLUMN_GEOSTYLE] = uniqueValue;
			tableData[i][TABLE_COLUMN_CAPTION] = uniqueItem.getCaption();
		}

		return tableData;
	}

	/**
	 * 表达式
	 * 
	 * @return m_fieldComboBox
	 */
	private JComboBox<String> getFieldComboBox(JComboBox<String> comboBox) {
		int count = datasetVector.getFieldCount();
		for (int j = 0; j < count; j++) {
			FieldInfo fieldInfo = datasetVector.getFieldInfos().get(j);
			if (fieldInfo.getType() != FieldType.TEXT) {
				String item = datasetVector.getName() + "." + fieldInfo.getName();
				comboBox.addItem(item);
			}
		}
		comboBox.addItem(MapViewProperties.getString("String_Combobox_Expression"));
		return comboBox;
	}

	/**
	 * 刷新table
	 */
	private void refreshTable() {
		DefaultTableModel defaultTableModel = new DefaultTableModel(getData(), nameStrings);
		tableUniqueInfo.setModel(defaultTableModel);
		initTableRender();
		tabbedPaneInfo.repaint();
	}

	/**
	 * 颜色方案改变时刷新颜色
	 */
	private void refreshColor() {
		if (comboboxColor != null) {
			int colorCount = ((Colors) comboboxColor.getSelectedItem())
					.getCount();
			Colors colors = (Colors) comboboxColor.getSelectedItem();
			int rangeCount = themeUnique.getCount();
			if (rangeCount > 0) {
				float ratio = (1f * colorCount) / (1f * rangeCount);
				setGeoStyleColor(themeUnique.getItem(0).getStyle(), colors.get(0));
				setGeoStyleColor(themeUnique.getItem(rangeCount - 1).getStyle(), colors.get(colorCount - 1));
				for (int i = 1; i < rangeCount - 1; i++) {
					int colorIndex = Math.round(i * ratio);
					if (colorIndex == colorCount) {
						colorIndex--;
					}
					setGeoStyleColor(themeUnique.getItem(i).getStyle(), colors.get(colorIndex));
				}
			}
		}
	}

	/**
	 * 根据当前数据集类型设置颜色方案
	 * 
	 * @param geoStyle 需要设置的风格
	 * @param color 设置的颜色
	 */
	private void setGeoStyleColor(GeoStyle geoStyle, Color color) {
		DatasetType datasetType = datasetVector.getType();
		if (CommonToolkit.DatasetTypeWrap.isPoint(datasetType) || CommonToolkit.DatasetTypeWrap.isLine(datasetType)) {
			geoStyle.setLineColor(color);
		} else if (CommonToolkit.DatasetTypeWrap.isRegion(datasetType)) {
			geoStyle.setFillForeColor(color);
		}
	}

	/**
	 * 下拉项发生变化时的事件处理类
	 * 
	 * @author Administrator
	 *
	 */
	class LocalComboBoxItemListener implements ItemListener {

		@Override
		public void itemStateChanged(ItemEvent e) {
			if (e.getStateChange() == ItemEvent.SELECTED) {
				if (e.getSource() == comboBoxExpression) {
					// sql表达式
					getSqlExpression(comboBoxExpression);
					// 修改表达式
					setFieldInfo();
				} else if (e.getSource() == comboBoxOffsetX) {
					getSqlExpression(comboBoxOffsetX);
					// 修改水平偏移量
					setOffsetX();
				} else if (e.getSource() == comboBoxOffsetY) {
					getSqlExpression(comboBoxOffsetY);
					// 修改垂直偏移量
					setOffsetY();
				} else if (e.getSource() == comboboxColor) {
					// 修改颜色方案
					refreshColor();
					UICommonToolkit.getLayersManager().getLayersTree().reload();
					map.refresh();
					refreshTable();
				} else if (e.getSource() == comboBoxOffsetUnity) {
					// 修改偏移量单位
					setOffsetUnity();
				}
			}
		}

		/**
		 * 字段表达式
		 */
		private void setFieldInfo() {
			String expression = (String) comboBoxExpression.getSelectedItem();
			expression = expression.substring(expression.lastIndexOf(".") + 1, expression.length());
			ThemeUnique theme = ThemeUnique.makeDefault(datasetVector, expression, ColorGradientType.YELLOWGREEN);
			map.getLayers().remove(themeUniqueLayer);
			themeUniqueLayer = map.getLayers().add(datasetVector, theme, true);
			UICommonToolkit.getLayersManager().getLayersTree().reload();
			themeUnique = (ThemeUnique) themeUniqueLayer.getTheme();
			textFieldUnique.setText(themeUnique.getItem(0).getCaption());
			refreshColor();
			map.refresh();
			refreshTable();
		}

		/**
		 * 修改水平偏移量
		 */
		private void setOffsetX() {
			String expression = (String) comboBoxExpression.getSelectedItem();
			if ("0".equals(expression)) {
				themeUnique.setOffsetX("0");
			} else {
				expression = expression.substring(expression.lastIndexOf(".") + 1, expression.length());
				((ThemeUnique) themeUniqueLayer.getTheme()).setOffsetX(expression);
			}
			map.refresh();
		}

		/**
		 * 修改垂直偏移量
		 */
		private void setOffsetY() {
			String expression = (String) comboBoxExpression.getSelectedItem();
			if ("0".equals(expression)) {
				themeUnique.setOffsetY("0");
			} else {
				expression = expression.substring(expression.lastIndexOf(".") + 1, expression.length());
				((ThemeUnique) themeUniqueLayer.getTheme()).setOffsetY(expression);
			}
			map.refresh();
		}

		private void setOffsetUnity() {
			if (MapViewProperties.getString("String_MapBorderLineStyle_LabelDistanceUnit").equals(comboBoxOffsetUnity.getSelectedItem().toString())) {
				((ThemeUnique) themeUniqueLayer.getTheme()).setOffsetFixed(true);
				labelOffsetXUnity.setText(MapViewProperties.getString("String_Combobox_MM"));
				labelOffsetYUnity.setText(MapViewProperties.getString("String_Combobox_MM"));
			} else {
				((ThemeUnique) themeUniqueLayer.getTheme()).setOffsetFixed(false);
				labelOffsetXUnity.setText(MapViewProperties.getString("String_FormSymbolDegree"));
				labelOffsetYUnity.setText(MapViewProperties.getString("String_FormSymbolDegree"));
			}
			map.refresh();
		}

		/**
		 * 获取表达式项
		 * 
		 * @param jComboBoxField
		 */
		private void getSqlExpression(JComboBox<String> jComboBoxField) {
			// 判断是否为“表达式”项
			if (MapViewProperties.getString("String_Combobox_Expression").equals(jComboBoxField.getSelectedItem())) {
				sqlDialog = new SQLExpressionDialog();
				int allItems = jComboBoxField.getItemCount();
				Dataset[] datasets = new Dataset[1];
				datasets[0] = datasetVector;
				DialogResult dialogResult = sqlDialog.showDialog(datasets);
				if (dialogResult == DialogResult.OK) {
					String filter = sqlDialog.getQueryParameter().getAttributeFilter();
					if (filter != null && !filter.trim().equals("")) {
						jComboBoxField.insertItemAt(filter, allItems - 1);
						jComboBoxField.setSelectedIndex(allItems - 1);
					}
				}

			}
		}
	}

	class LocalActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == buttonVisble) {
				// 批量修改单值段的可见状态
				setItemVisble();
			} else if (e.getSource() == buttonGeoSytle) {
				// 批量修改单值段的符号方案
				setItemGeoSytle();
			} else if (e.getSource() == buttonAdd) {
				// 添加单值段
			} else if (e.getSource() == buttonDelete) {
				// 删除单值段
				deleteItem();
			} else if (e.getSource() == buttonAntitone) {
				// 颜色方案反序
				setGeoStyleAntitone();
			}
		}

		/**
		 * 删除单值段
		 */
		private void deleteItem() {
			int[] selectedRow = tableUniqueInfo.getSelectedRows();
			themeUnique = (ThemeUnique) themeUniqueLayer.getTheme();
			if (selectedRow.length == 1) {
				String caption = (String) tableUniqueInfo.getValueAt(selectedRow[0], TABLE_COLUMN_CAPTION);
				int themeIndex = themeUnique.indexOf(caption);
				ThemeUniqueItem item = themeUnique.getItem(themeIndex);
				deleteItems.add(item);
				themeUnique.remove(themeIndex);
				
			} else {
				for (int i = 0; i < selectedRow.length; i++) {
					String caption = (String) tableUniqueInfo.getValueAt(selectedRow[i], TABLE_COLUMN_CAPTION);
					int themeIndex = themeUnique.indexOf(caption);
					ThemeUniqueItem item = themeUnique.getItem(themeIndex);
					deleteItems.add(item);
					themeUnique.remove(themeIndex);
				}
			}
			map.refresh();
			refreshTable();
			UICommonToolkit.getLayersManager().getLayersTree().reload();
		}

		/**
		 * 设置颜色方案与当前颜色方案反序
		 */
		private void setGeoStyleAntitone() {
			themeUnique = (ThemeUnique) themeUniqueLayer.getTheme();
			themeUnique.reverseStyle();
			map.refresh();
			refreshTable();
			UICommonToolkit.getLayersManager().getLayersTree().reload();
		}

		/**
		 * 设置单值项是否可见
		 */
		private void setItemVisble() {
			int[] selectedRow = tableUniqueInfo.getSelectedRows();
			if (selectedRow.length == 1) {
				resetVisible(selectedRow[0]);
			} else {
				for (int i = 0; i < selectedRow.length; i++) {
					resetVisible(selectedRow[i]);
				}
			}
			map.refresh();
			refreshTable();
			UICommonToolkit.getLayersManager().getLayersTree().reload();
		}

		/**
		 * 重置可见选项
		 * 
		 * @param selectRow 要重置的行
		 */
		private void resetVisible(int selectRow) {
			boolean visible = ((ThemeUnique) themeUniqueLayer.getTheme()).getItem(selectRow).isVisible();
			if (visible) {
				((ThemeUnique) themeUniqueLayer.getTheme()).getItem(selectRow).setVisible(false);
				((VisibleValue) tableUniqueInfo.getValueAt(selectRow, 0)).setIcon(InternalImageIconFactory.INVISIBLE);
			} else {
				((ThemeUnique) themeUniqueLayer.getTheme()).getItem(selectRow).setVisible(true);
				((VisibleValue) tableUniqueInfo.getValueAt(selectRow, 0)).setIcon(InternalImageIconFactory.VISIBLE);
			}
		}

		/**
		 * 批量设置文本风格
		 */
		private void setItemGeoSytle() {
			int[] selectedRow = tableUniqueInfo.getSelectedRows();
			SymbolDialog textStyleDialog = new SymbolDialog();
			String name = tableUniqueInfo.getColumnName(TABLE_COLUMN_VISIBLE);
			int width = tableUniqueInfo.getColumn(name).getWidth();
			int height = tableUniqueInfo.getTableHeader().getHeight();
			int x = tableUniqueInfo.getLocationOnScreen().x + width;
			int y = tableUniqueInfo.getLocationOnScreen().y - height;
			textStyleDialog.setLocation(x, y);
			Resources resources = Application.getActiveApplication().getWorkspace().getResources();
			SymbolType symbolType = null;
			GeoStyle geoStyle = new GeoStyle();
			if (CommonToolkit.DatasetTypeWrap.isPoint(datasetVector.getType())) {
				symbolType = SymbolType.MARKER;
			} else if (CommonToolkit.DatasetTypeWrap.isLine(datasetVector.getType())) {
				symbolType = SymbolType.LINE;
			} else if (CommonToolkit.DatasetTypeWrap.isRegion(datasetVector.getType())) {
				symbolType = SymbolType.FILL;
			}
			DialogResult dialogResult = textStyleDialog.showDialog(resources, geoStyle, symbolType);
			if (dialogResult.equals(DialogResult.OK)) {
				GeoStyle nowGeoStyle = textStyleDialog.getStyle();
				if (selectedRow.length == 1) {
					resetGeoSytle(selectedRow[0], nowGeoStyle, symbolType);
				} else {
					for (int i = 0; i < selectedRow.length; i++) {
						resetGeoSytle(selectedRow[i], nowGeoStyle, symbolType);
					}
				}
			}
			map.refresh();
			refreshTable();
			UICommonToolkit.getLayersManager().getLayersTree().reload();
		}

		/**
		 * 重置文本风格
		 * 
		 * @param selectRow 要重置文本风格的行
		 * @param nowGeoStyle 新的文本风格
		 * @param symbolType 文本的风格类型
		 */
		private void resetGeoSytle(int selectRow, GeoStyle nowGeoStyle, SymbolType symbolType) {
			ThemeUniqueItem item = ((ThemeUnique) themeUniqueLayer.getTheme()).getItem(selectRow);
			item.setStyle(nowGeoStyle);
			UniqueValue nowValue = (UniqueValue) tableUniqueInfo.getValueAt(selectRow, 1);
			if (symbolType == SymbolType.MARKER || symbolType == SymbolType.LINE) {
				nowValue.setColor(item.getStyle().getLineColor());
			} else {
				nowValue.setColor(item.getStyle().getFillForeColor());
			}
		}

	}

}
