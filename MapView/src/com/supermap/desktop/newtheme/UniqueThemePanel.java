package com.supermap.desktop.newtheme;

import java.awt.Color;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import com.supermap.desktop.mapview.MapViewProperties;
import com.supermap.desktop.ui.controls.InternalImageIconFactory;

public class UniqueThemePanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private JLabel labelUniqueTheme = new JLabel("");
	public UniqueThemePanel() {
		initComponents();
		initResources();
	}

	private void initComponents() {
		setBorder(new LineBorder(Color.LIGHT_GRAY));
		setBackground(Color.WHITE);

		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
								.addGap(20)
								.addComponent(labelUniqueTheme)
								.addContainerGap(368, Short.MAX_VALUE))
				);
		groupLayout.setVerticalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
								.addGap(24)
								.addComponent(labelUniqueTheme)
								.addContainerGap(223, Short.MAX_VALUE))
				);
		setLayout(groupLayout);
	}

	private void initResources() {
		labelUniqueTheme.setIcon(InternalImageIconFactory.THEMEGUIDE_UNIQUE);
		labelUniqueTheme.setText(MapViewProperties.getString("String_Theme_Unique"));
		labelUniqueTheme.setVerticalTextPosition(JLabel.BOTTOM);
		labelUniqueTheme.setHorizontalTextPosition(JLabel.CENTER);
	}

	private void registListener() {

	}

	private void unregistListener() {

	}
}
