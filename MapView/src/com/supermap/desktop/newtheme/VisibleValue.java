package com.supermap.desktop.newtheme;

import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.supermap.desktop.ui.controls.InternalImageIconFactory;
import com.supermap.mapping.ThemeLabelItem;
import com.supermap.mapping.ThemeRangeItem;
import com.supermap.mapping.ThemeUniqueItem;

/**
 * 包含右Panel的类,用于table渲染
 * 
 * @author xie
 * 
 */
public class VisibleValue extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel label;
	private ThemeUniqueItem uniqueItem;
	private ThemeRangeItem rangeItem;
	private ThemeLabelItem labelItem;

	public VisibleValue(ThemeUniqueItem uniqueItem) {
		super();
		this.uniqueItem = uniqueItem;
		this.setLayout(new GridLayout(1, 1));
		add(getLabel());
	}

	public VisibleValue(ThemeRangeItem rangeItem) {
		super();
		this.rangeItem = rangeItem;
		this.setLayout(new GridLayout(1, 1));
		add(getLabel());
	}

	public VisibleValue(ThemeLabelItem labelItem) {
		super();
		this.labelItem = labelItem;
		this.setLayout(new GridLayout(1, 1));
		add(getLabel());
	}

	/**
	 * 颜色Label
	 * 
	 * @return
	 */
	public JLabel getLabel() {
		if (null != uniqueItem) {
			if (uniqueItem.isVisible()) {
				label = new JLabel(InternalImageIconFactory.VISIBLE);
			} else {
				label = new JLabel(InternalImageIconFactory.INVISIBLE);
			}
		}
		if (null != rangeItem) {
			if (rangeItem.isVisible()) {
				label = new JLabel(InternalImageIconFactory.VISIBLE);
			} else {
				label = new JLabel(InternalImageIconFactory.INVISIBLE);
			}
		}
		if (null != labelItem) {
			if (labelItem.isVisible()) {
				label = new JLabel(InternalImageIconFactory.VISIBLE);
			} else {
				label = new JLabel(InternalImageIconFactory.INVISIBLE);
			}
		}
		return label;
	}

	public ImageIcon getIcon() {
		return (ImageIcon) label.getIcon();
	}

	public void setIcon(ImageIcon icon) {
		label.setIcon(icon);
	}

	public ThemeUniqueItem getUniqueItem() {
		return uniqueItem;
	}

	public void setUniqueItem(ThemeUniqueItem uniqueItem) {
		this.uniqueItem = uniqueItem;
	}

	public ThemeRangeItem getRangeItem() {
		return rangeItem;
	}

	public void setRangeItem(ThemeRangeItem rangeItem) {
		this.rangeItem = rangeItem;
	}

	public ThemeLabelItem getLabelItem() {
		return labelItem;
	}

	public void setLabelItem(ThemeLabelItem labelItem) {
		this.labelItem = labelItem;
	}

}
