package com.supermap.desktop.newtheme;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.supermap.desktop.mapview.MapViewProperties;
import com.supermap.desktop.ui.controls.GridBagConstraintsHelper;
import javax.swing.border.LineBorder;
import java.awt.Color;

public class ThemeMainContainer extends JPanel {

	private static final long serialVersionUID = 1L;
	private JLabel labelThemeLayer = new JLabel();
	private JComboBox<String> comboBoxThemeLayer = new JComboBox<String>();
	private JPanel panelThemeInfo = new JPanel();
	private JCheckBox checkBoxRefreshAtOnce = new JCheckBox();
	private JButton buttonApply = new JButton();

	public ThemeMainContainer() {
		initComponents();
		initResources();
	}

	private void initResources() {
		this.labelThemeLayer.setText(MapViewProperties.getString("String_Themelayers"));
		checkBoxRefreshAtOnce.setSelected(true);
		this.checkBoxRefreshAtOnce.setText(MapViewProperties.getString("String_RefreshAtOnce"));
		this.buttonApply.setText(MapViewProperties.getString("String_Button_Apply"));
	}

	private void initComponents() {
		initContentPanel();
	}

	private void initContentPanel() {
		GridBagLayout layout = new GridBagLayout();
		this.setLayout(layout);
		this.add(labelThemeLayer,        new GridBagConstraintsHelper(0, 0, 1, 1).setWeight(3, 0).setInsets(5,20,5,20).setAnchor(GridBagConstraints.WEST).setIpad(30, 0));
		comboBoxThemeLayer.setEditable(true);
		this.add(comboBoxThemeLayer,     new GridBagConstraintsHelper(1, 0, 1, 1).setWeight(3, 0).setInsets(5).setAnchor(GridBagConstraints.CENTER).setIpad(30, 0).setFill(GridBagConstraints.HORIZONTAL));
		panelThemeInfo.setBorder(new LineBorder(Color.LIGHT_GRAY));
		this.add(panelThemeInfo,         new GridBagConstraintsHelper(0, 1, 2, 1).setWeight(3, 3).setInsets(5).setAnchor(GridBagConstraints.CENTER).setIpad(0, 0).setFill(GridBagConstraints.BOTH));
		this.add(checkBoxRefreshAtOnce, new GridBagConstraintsHelper(0, 2, 1, 1).setWeight(3, 0).setInsets(5).setAnchor(GridBagConstraints.CENTER).setIpad(30, 0));
		this.add(buttonApply,            new GridBagConstraintsHelper(1, 2, 1, 1).setWeight(3, 0).setInsets(5).setAnchor(GridBagConstraints.CENTER).setIpad(30, 0));
		
	}

	public JPanel getPanelThemeInfo() {
		return panelThemeInfo;
	}

	public void setPanelThemeInfo(JPanel panelThemeInfo) {
		this.panelThemeInfo = panelThemeInfo;
	}

	private void registActionListener() {
		
	}

	private void unregistActionListener() {

	}
}
