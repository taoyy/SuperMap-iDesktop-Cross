package com.supermap.desktop.newtheme;

import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.EventObject;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.table.TableCellEditor;

import com.supermap.desktop.Application;
import com.supermap.desktop.ui.controls.InternalImageIconFactory;
import com.supermap.mapping.ThemeLabel;
import com.supermap.mapping.ThemeLabelItem;
import com.supermap.mapping.ThemeRangeItem;
import com.supermap.mapping.ThemeUniqueItem;

/**
 * table的填充和控制
 * 
 * @author xie
 *
 */
public class JPanelCellEditorVisible extends JPanel implements TableCellEditor, ActionListener, MouseListener {
	private static final long serialVersionUID = 1L;

	int row;

	int col;

	private JLabel label;

	private JTable jTable;

	private int themeType;
	public static final int THEME_UNIQUE_TYPE = 0;
	public static final int THEME_RANGE_TYPE = 1;
	public static final int THEME_LABEL_TYPE = 2;

	public JPanelCellEditorVisible(int themeType) {
		this.themeType = themeType;
		this.setLayout(new GridLayout(1, 1));
		this.setBorder(BorderFactory.createEmptyBorder(0, 0, 2, 2));
		label = new JLabel();
		label.addMouseListener(this);
		this.add(label);
	}

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		ImageIcon icon = ((VisibleValue) table.getValueAt(row, column)).getIcon();
		label.setIcon(icon);
		this.jTable = table;
		this.row = row;
		this.col = column;
		return this;
	}

	@Override
	public Object getCellEditorValue() {

		return (UniqueValue) jTable.getValueAt(row, col);
	}

	@Override
	public boolean isCellEditable(EventObject anEvent) {
		return true;
	}

	@Override
	public boolean shouldSelectCell(EventObject anEvent) {
		return true;
	}

	@Override
	public boolean stopCellEditing() {
		return true;
	}

	@Override
	public void cancelCellEditing() {
		// TODO something
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO something
	}

	@Override
	public void addCellEditorListener(CellEditorListener l) {
		// TODO something
	}

	@Override
	public void removeCellEditorListener(CellEditorListener l) {
		// TODO something
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (1 == e.getClickCount()) {
			try {
				if (this.themeType == THEME_UNIQUE_TYPE) {
					boolean visible = ((VisibleValue) jTable.getValueAt(row, col)).getUniqueItem().isVisible();
					setItemVisible(visible);
				} else if (this.themeType == THEME_RANGE_TYPE) {
					boolean visible = ((VisibleValue) jTable.getValueAt(row, col)).getRangeItem().isVisible();
					setItemVisible(visible);
				} else if (this.themeType == THEME_LABEL_TYPE) {
					boolean visible = ((VisibleValue) jTable.getValueAt(row, col)).getLabelItem().isVisible();
					setItemVisible(visible);
				}
			} catch (Exception ex) {
				Application.getActiveApplication().getOutput().output(ex);
			}
		}
	}

	private void setItemVisible(boolean visible) {
		if (visible) {
			visible = false;
			((VisibleValue) jTable.getValueAt(row, col)).setIcon(InternalImageIconFactory.INVISIBLE);
		} else {
			visible = true;
			((VisibleValue) jTable.getValueAt(row, col)).setIcon(InternalImageIconFactory.VISIBLE);
		}
		getTableCellEditorComponent(jTable, null, true, row, col);
		firePropertyChange("Visible", null, visible);
		JPanelCellEditorVisible.this.stopCellEditing();
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO something

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO something

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO something

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO something

	}

}
