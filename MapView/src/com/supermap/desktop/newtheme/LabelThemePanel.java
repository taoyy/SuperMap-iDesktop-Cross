package com.supermap.desktop.newtheme;

import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;

import java.awt.Color;

import javax.swing.border.LineBorder;
import javax.swing.JLabel;

import com.supermap.desktop.mapview.MapViewProperties;
import com.supermap.desktop.ui.controls.InternalImageIconFactory;

public class LabelThemePanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel labelUniformThmeme = new JLabel("");
	JLabel labelRangeTheme = new JLabel("");

	public LabelThemePanel() {
		initComponents();
		initResources();
	}

	private void initComponents() {
		setBorder(new LineBorder(Color.LIGHT_GRAY));
		setBackground(Color.WHITE);

		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(26)
					.addComponent(labelUniformThmeme)
					.addGap(13)
					.addComponent(labelRangeTheme)
					.addContainerGap(250, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(20)
							.addComponent(labelUniformThmeme))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(21)
							.addComponent(labelRangeTheme)))
					.addContainerGap(226, Short.MAX_VALUE))
		);
		setLayout(groupLayout);
	}

	private void initResources() {
		labelUniformThmeme.setIcon(InternalImageIconFactory.THEMEGUIDE_UNIFORM);
		labelUniformThmeme.setText(MapViewProperties.getString("String_ThemeLabelUniformItem"));
		labelUniformThmeme.setVerticalTextPosition(JLabel.BOTTOM);
		labelUniformThmeme.setHorizontalTextPosition(JLabel.CENTER);
		labelRangeTheme.setIcon(InternalImageIconFactory.THEMEGUIDE_RANGES);
		labelRangeTheme.setText(MapViewProperties.getString("String_ThemeLabelRangeItem"));
		labelRangeTheme.setVerticalTextPosition(JLabel.BOTTOM);
		labelRangeTheme.setHorizontalTextPosition(JLabel.CENTER);
	}

	private void registListener() {

	}

	private void unregistListener() {

	}

}
