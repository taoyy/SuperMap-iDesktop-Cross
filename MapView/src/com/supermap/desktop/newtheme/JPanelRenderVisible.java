package com.supermap.desktop.newtheme;

import java.awt.Component;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 * 包含Panel的类,用于table渲染中某项是否可见
 * 
 * @author xie
 * 
 */
public class JPanelRenderVisible extends JPanel implements TableCellRenderer {
	private static final long serialVersionUID = 1L;

	private JLabel label;


	public JPanelRenderVisible() {
		super();
		this.setLayout(new GridLayout(1, 1));
		this.setBorder(BorderFactory.createEmptyBorder(0, 0, 2, 2));
		label = new JLabel();
		this.add(label);
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		ImageIcon icon = ((VisibleValue) table.getValueAt(row, column)).getIcon();
		label.setIcon(icon);
		return this;
	}
}
