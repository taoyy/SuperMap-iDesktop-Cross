package com.supermap.desktop.newtheme;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import com.supermap.desktop.controls.ControlsProperties;
import com.supermap.desktop.mapview.MapViewProperties;
import com.supermap.desktop.ui.controls.GridBagConstraintsHelper;

public class ThemeLabelPropertyPanel extends JPanel{
	private static final long serialVersionUID = 1L;
	private JLabel labelLabelExpression = new JLabel();
	private JComboBox<String> comboBoxLabelExpression = new JComboBox<String>();
	// panelBackgroundSet
	private JLabel labelBGShape = new JLabel();
	private JComboBox<String> comboBoxBGShape = new JComboBox<String>();
	private JLabel labelBGSytle = new JLabel();
	private JButton buttonBGStyle = new JButton();
	// panelLabelOffset
	private JLabel labelOffsetUnity = new JLabel();
	private JComboBox<String> comboBoxOffsetUnity = new JComboBox<String>();
	private JLabel labelOffsetX = new JLabel();
	private JComboBox<String> comboBoxOffsetX = new JComboBox<String>();
	private JLabel labelOffsetY = new JLabel();
	private JComboBox<String> comboBoxOffsetY = new JComboBox<String>();
	// panelLabelEffectSet
	private JCheckBox checkBoxFlowVisual = new JCheckBox();
	private JCheckBox checkBoxShowSubscription = new JCheckBox();
	private JLabel labelShowSubscription = new JLabel("?");
	private JCheckBox checkBoxShowSmallLabel = new JCheckBox();
	private JCheckBox checkBoxShowLabelVertical = new JCheckBox();
	private JLabel labelShowLabelVertical = new JLabel("?");
	private JCheckBox checkBoxAutoAvoidance = new JCheckBox();
	private JComboBox<String> comboBoxAutoAvoidance = new JComboBox<String>();
	private JCheckBox checkBoxDraftLine = new JCheckBox();
	private JComboBox<String> comboBoxDraftLine = new JComboBox<String>();
	private JLabel labelTextPrecision = new JLabel();
	private JComboBox<String> comboBoxTextPrecision = new JComboBox<String>();
	public ThemeLabelPropertyPanel(){
		initComponents();
		initResources();
	}
	private void initResources(){
		this.labelLabelExpression.setText(MapViewProperties.getString("String_label_Expression"));
		this.labelBGShape.setText(MapViewProperties.getString("String_BackShape"));
		this.labelBGSytle.setText(MapViewProperties.getString("String_BackStyle"));
		this.labelOffsetUnity.setText(MapViewProperties.getString("String_LabelOffsetUnit"));
		this.labelOffsetX.setText(MapViewProperties.getString("String_LabelOffsetX"));
		this.labelOffsetY.setText(MapViewProperties.getString("String_LabelOffsetY"));
		this.checkBoxFlowVisual.setText(MapViewProperties.getString("String_CheckBox_ShowFlow"));
		this.checkBoxShowSubscription.setText(MapViewProperties.getString("String_TextExpression"));
		this.labelShowSubscription.setToolTipText(MapViewProperties.getString("String_TextExpression_InformationText"));
		this.checkBoxShowSmallLabel.setText(MapViewProperties.getString("String_SmallGeometry"));
		this.checkBoxShowLabelVertical.setText(MapViewProperties.getString("String_IsVertical"));
		this.labelShowLabelVertical.setToolTipText(MapViewProperties.getString("String_IsVertical_InformationText"));
		this.checkBoxAutoAvoidance.setText(MapViewProperties.getString("String_CheckBox_AutoAvoid"));
		this.checkBoxDraftLine.setText(MapViewProperties.getString("String_ShowLeaderLine"));
		this.labelTextPrecision.setText(MapViewProperties.getString("String_Precision"));
		this.buttonBGStyle.setText(ControlsProperties.getString("String_Button_Setting"));
	}
	
	private void initComponents() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		this.setLayout(gridBagLayout);
		JPanel panelBGSet = new JPanel();
		panelBGSet
				.setBorder(new TitledBorder(null, MapViewProperties.getString("String_BackShapeSetting"), TitledBorder.LEADING, TitledBorder.TOP, null, null));
		JPanel panelLabelOffset = new JPanel();
		panelLabelOffset
				.setBorder(new TitledBorder(null, MapViewProperties.getString("String_LabelOffset"), TitledBorder.LEADING, TitledBorder.TOP, null, null));
		JPanel panelLabelEffectSet = new JPanel();
		panelLabelEffectSet.setBorder(new TitledBorder(null, MapViewProperties.getString("String_EffectOption"), TitledBorder.LEADING, TitledBorder.TOP, null,
				null));
		initPanelBGSet(panelBGSet);
		initPanelLabelOffset(panelLabelOffset);
		initPanelLabelEffectSet(panelLabelEffectSet);
		//@formatter:off
		this.add(this.labelLabelExpression,    new GridBagConstraintsHelper(0, 0, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2, 20, 2, 10).setIpad(30, 0));
		this.comboBoxLabelExpression.setEditable(true);
		this.add(this.comboBoxLabelExpression, new GridBagConstraintsHelper(1, 0, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2, 5, 2, 5).setIpad(30, 0));
		this.add(panelBGSet, new GridBagConstraintsHelper(0, 1, 2, 1).setAnchor(GridBagConstraints.CENTER).setInsets(5).setWeight(1, 0).setFill(GridBagConstraints.HORIZONTAL));
		this.add(panelLabelOffset, new GridBagConstraintsHelper(0, 2, 2, 1).setAnchor(GridBagConstraints.CENTER).setInsets(5).setWeight(1,0).setFill(GridBagConstraints.HORIZONTAL));
		this.add(panelLabelEffectSet, new GridBagConstraintsHelper(0, 3, 2, 1).setAnchor(GridBagConstraints.CENTER).setInsets(5).setWeight(1, 0).setFill(GridBagConstraints.HORIZONTAL));
		//@formatter:on
	}
	
	private void initPanelBGSet(JPanel panelBGSet) {
		//@formatter:off
		panelBGSet.setLayout(new GridBagLayout());
		panelBGSet.add(this.labelBGShape,    new GridBagConstraintsHelper(0, 0, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		this.comboBoxBGShape.setEditable(true);
		panelBGSet.add(this.comboBoxBGShape, new GridBagConstraintsHelper(1, 0, 1, 1).setAnchor(GridBagConstraints.CENTER).setWeight(1, 0).setInsets(2,10,2,10).setIpad(30, 0));
		panelBGSet.add(this.labelBGSytle,    new GridBagConstraintsHelper(0, 1, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		panelBGSet.add(this.buttonBGStyle,   new GridBagConstraintsHelper(1, 1, 1, 1).setAnchor(GridBagConstraints.CENTER).setWeight(1, 0).setInsets(2,10,2,10).setIpad(30, 0));
		//@formatter:on
	}
	
	private void initPanelLabelOffset(JPanel panelLabelOffset) {
		//@formatter:off
		panelLabelOffset.setLayout(new GridBagLayout());
		panelLabelOffset.add(this.labelOffsetUnity,    new GridBagConstraintsHelper(0, 0, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		this.comboBoxOffsetUnity.setEditable(true);
		panelLabelOffset.add(this.comboBoxOffsetUnity, new GridBagConstraintsHelper(1, 0, 1, 1).setAnchor(GridBagConstraints.CENTER).setWeight(1, 0).setInsets(2,10,2,10).setIpad(30, 0));
		panelLabelOffset.add(this.labelOffsetX,        new GridBagConstraintsHelper(0, 1, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		this.comboBoxOffsetX.setEditable(true);
		panelLabelOffset.add(this.comboBoxOffsetX,     new GridBagConstraintsHelper(1, 1, 1, 1).setAnchor(GridBagConstraints.CENTER).setWeight(1, 0).setInsets(2,10,2,10).setIpad(30, 0));
		panelLabelOffset.add(this.labelOffsetY,        new GridBagConstraintsHelper(0, 2, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		this.comboBoxOffsetY.setEditable(true);
		panelLabelOffset.add(this.comboBoxOffsetY,     new GridBagConstraintsHelper(1, 2, 1, 1).setAnchor(GridBagConstraints.CENTER).setWeight(1, 0).setInsets(2,10,2,10).setIpad(30, 0));
		//@formatter:on
	}
	private void initPanelLabelEffectSet(JPanel panelLabelEffectSet) {
		//@formatter:off
		panelLabelEffectSet.setLayout(new GridBagLayout());
		panelLabelEffectSet.add(this.checkBoxFlowVisual,       new GridBagConstraintsHelper(0, 0, 2, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		panelLabelEffectSet.add(this.checkBoxShowSubscription, new GridBagConstraintsHelper(2, 0, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10).setIpad(30, 0));
		panelLabelEffectSet.add(this.labelShowSubscription,    new GridBagConstraintsHelper(3, 0, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		panelLabelEffectSet.add(this.checkBoxShowSmallLabel,   new GridBagConstraintsHelper(0, 1, 2, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10).setIpad(30, 0));
		panelLabelEffectSet.add(this.checkBoxShowLabelVertical,new GridBagConstraintsHelper(2, 1, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		panelLabelEffectSet.add(this.labelShowLabelVertical,   new GridBagConstraintsHelper(3, 1, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10).setIpad(30, 0));
		panelLabelEffectSet.add(this.checkBoxAutoAvoidance,    new GridBagConstraintsHelper(0, 2, 2, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		this.comboBoxAutoAvoidance.setEditable(true);
		panelLabelEffectSet.add(this.comboBoxAutoAvoidance,    new GridBagConstraintsHelper(2, 2, 2, 1).setAnchor(GridBagConstraints.CENTER).setWeight(1, 0).setInsets(2,10,2,10).setIpad(30, 0));
		panelLabelEffectSet.add(this.checkBoxDraftLine,        new GridBagConstraintsHelper(0, 3, 2, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		this.comboBoxDraftLine.setEditable(true);
		panelLabelEffectSet.add(this.comboBoxDraftLine,        new GridBagConstraintsHelper(2, 3, 2, 1).setAnchor(GridBagConstraints.CENTER).setWeight(1, 0).setInsets(2,10,2,10).setIpad(30, 0));
		panelLabelEffectSet.add(this.labelTextPrecision,       new GridBagConstraintsHelper(0, 4, 2, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		this.comboBoxTextPrecision.setEditable(true);
		panelLabelEffectSet.add(this.comboBoxTextPrecision,    new GridBagConstraintsHelper(2, 4, 2, 1).setAnchor(GridBagConstraints.CENTER).setWeight(1, 0).setInsets(2,10,2,10).setIpad(30, 0));
		//@formatter:on
	}
}
