package com.supermap.desktop.newtheme;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.supermap.desktop.mapview.MapViewProperties;
import com.supermap.desktop.ui.controls.GridBagConstraintsHelper;

public class ThemeLabelAdvancePanel extends JPanel {

	private static final long serialVersionUID = 1L;
	// panelAdvanced
	private JCheckBox checkBoxRotateLabel = new JCheckBox();
	// panelRotateLabel
	private JCheckBox checkBoxFixedFontAngl = new JCheckBox();
	private JCheckBox checkBoxRemoveRepeatLabel = new JCheckBox();
	private JLabel labelLineDirection = new JLabel();
	private JComboBox<String> comboBoxLineDirection = new JComboBox<String>();
	private JLabel labelFontSpace = new JLabel();
	private JSpinner spinnerFontSpace = new JSpinner();
	private JLabel labelRepeatInterval = new JLabel();
	private JTextField textFieldRepeatInterval = new JTextField();
	private JLabel labelWordDisplay = new JLabel();
	private JComboBox<String> comboBoxWordDisplay = new JComboBox<String>();
	private JCheckBox checkBoxRepeatIntervalFixed = new JCheckBox();
	// panelTextFontSet
	private JLabel labelOverLength = new JLabel();
	private JComboBox<String> comboBoxOverLength = new JComboBox<String>();
	private JLabel labelFontCount = new JLabel();
	private JSpinner spinnerFontCount = new JSpinner();
	// panelFontHeight
	private JLabel labelMaxFontHeight = new JLabel();
	private JTextField textFieldMaxFontHeight = new JTextField();
	private JLabel labelMaxFontHeightUnity = new JLabel();
	private JLabel labelMinFontHeight = new JLabel();
	private JTextField textFieldMinFontHeight = new JTextField();
	private JLabel labelMinFontHeightUnity = new JLabel();
	// panelFontWide
	private JLabel labelMaxFontWidth = new JLabel();
	private JTextField textFieldMaxFontWidth = new JTextField();
	private JLabel labelMaxFontWidthUnity = new JLabel();
	private JLabel labelMinFontWidth = new JLabel();
	private JTextField textFieldMinFontWidth = new JTextField();
	private JLabel labelMinFontWidthUnity = new JLabel();
	// panelTextExtentInflation
	private JLabel labelHorizontal = new JLabel();
	private JTextField textFieldHorizontal = new JTextField();
	private JLabel labelHorizontalUnity = new JLabel();
	private JLabel labelVertical = new JLabel();
	private JTextField textFieldVertical = new JTextField();
	private JLabel labelVerticalUnity = new JLabel();

	public ThemeLabelAdvancePanel() {
		initComponents();
		initResources();
	}

	private void initComponents() {
		JPanel panelRotateLabel = new JPanel();
		panelRotateLabel.setBorder(new LineBorder(Color.LIGHT_GRAY));
		JPanel panelTextFontSet = new JPanel();
		panelTextFontSet
				.setBorder(new TitledBorder(null, MapViewProperties.getString("String_CharLimited"), TitledBorder.LEADING, TitledBorder.TOP, null, null));
		JPanel panelFontHeight = new JPanel();
		panelFontHeight.setBorder(new TitledBorder(null, MapViewProperties.getString("String_LimitedHeight"), TitledBorder.LEADING, TitledBorder.TOP, null,
				null));
		JPanel panelFontWide = new JPanel();
		panelFontWide.setBorder(new TitledBorder(null, MapViewProperties.getString("String_WidthLimited"), TitledBorder.LEADING, TitledBorder.TOP, null, null));
		JPanel panelTextExtentInflation = new JPanel();
		panelTextExtentInflation.setBorder(new TitledBorder(null, MapViewProperties.getString("String_TextExtentInflation"), TitledBorder.LEADING,
				TitledBorder.TOP, null, null));
		initPanelRotateLabel(panelRotateLabel);
		initPanelTextFontSet(panelTextFontSet);
		initPanelFontHeight(panelFontHeight);
		initPanelFontWidth(panelFontWide);
		initPanelTextExtentInflation(panelTextExtentInflation);
		//@formatter:off
		this.setLayout(new GridBagLayout());
		this.add(this.checkBoxRotateLabel, new GridBagConstraintsHelper(0, 0, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		this.add(panelRotateLabel,         new GridBagConstraintsHelper(0, 1, 2, 1).setAnchor(GridBagConstraints.CENTER).setWeight(1, 0).setInsets(2,10,2,10).setFill(GridBagConstraints.HORIZONTAL));
		this.add(panelTextFontSet,         new GridBagConstraintsHelper(0, 2, 2, 1).setAnchor(GridBagConstraints.CENTER).setWeight(1, 0).setInsets(2,10,2,10).setFill(GridBagConstraints.HORIZONTAL));
		this.add(panelFontHeight,          new GridBagConstraintsHelper(0, 3, 2, 1).setAnchor(GridBagConstraints.CENTER).setWeight(1, 0).setInsets(2,10,2,10).setFill(GridBagConstraints.HORIZONTAL));
		this.add(panelFontWide,            new GridBagConstraintsHelper(0, 4, 2, 1).setAnchor(GridBagConstraints.CENTER).setWeight(1, 0).setInsets(2,10,2,10).setFill(GridBagConstraints.HORIZONTAL));
		this.add(panelTextExtentInflation, new GridBagConstraintsHelper(0, 5, 2, 1).setAnchor(GridBagConstraints.CENTER).setWeight(1, 0).setInsets(2,10,2,10).setFill(GridBagConstraints.HORIZONTAL));
		//@formatter:on
	}

	private void initResources() {
		this.checkBoxRotateLabel.setText(MapViewProperties.getString("String_AlongLine"));
		this.checkBoxFixedFontAngl.setText(MapViewProperties.getString("String_CheckBox_IsTextAngleFixed"));
		this.checkBoxRemoveRepeatLabel.setText(MapViewProperties.getString("String_RemoveRepeat"));
		this.labelLineDirection.setText(MapViewProperties.getString("String_LineDirection"));
		this.labelFontSpace.setText(MapViewProperties.getString("String_SpaceRatio"));
		this.labelRepeatInterval.setText(MapViewProperties.getString("String_RepeatInterval"));
		this.labelWordDisplay.setText(MapViewProperties.getString("String_Label_AlongLineCulture"));
		this.checkBoxRepeatIntervalFixed.setText(MapViewProperties.getString("String_RepeatIntervalFixed"));

		this.labelOverLength.setText(MapViewProperties.getString("String_OverLengthLabelMode"));
		this.labelFontCount.setText(MapViewProperties.getString("String_CharCount"));

		this.labelMaxFontHeight.setText(MapViewProperties.getString("String_MaxHeight"));
		this.labelMaxFontHeightUnity.setText(MapViewProperties.getString("String_Combobox_MM"));
		this.labelMinFontHeight.setText(MapViewProperties.getString("String_MinHeight"));
		this.labelMinFontHeightUnity.setText(MapViewProperties.getString("String_Combobox_MM"));

		this.labelMaxFontWidth.setText(MapViewProperties.getString("String_MaxWidth"));
		this.labelMaxFontWidthUnity.setText(MapViewProperties.getString("String_Combobox_MM"));
		this.labelMinFontWidth.setText(MapViewProperties.getString("String_MinHeight"));
		this.labelMinFontWidthUnity.setText(MapViewProperties.getString("String_Combobox_MM"));

		this.labelHorizontal.setText(MapViewProperties.getString("String_TextExtentWidth"));
		this.labelHorizontalUnity.setText(MapViewProperties.getString("String_FormSymbolDegree"));
		this.labelVertical.setText(MapViewProperties.getString("String_TextExtentHeight"));
		this.labelVerticalUnity.setText(MapViewProperties.getString("String_FormSymbolDegree"));
	}

	private void initPanelFontHeight(JPanel panelFontHeight) {
		//@formatter:off
		panelFontHeight.setLayout(new GridBagLayout());
		panelFontHeight.add(this.labelMaxFontHeight,     new GridBagConstraintsHelper(0, 0, 2, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		this.textFieldMaxFontHeight.setText("0");
		panelFontHeight.add(this.textFieldMaxFontHeight, new GridBagConstraintsHelper(2, 0, 1, 1).setAnchor(GridBagConstraints.EAST).setWeight(1, 0).setInsets(2,10,2,10).setIpad(50, 0));
		panelFontHeight.add(this.labelMaxFontHeightUnity,new GridBagConstraintsHelper(3, 0, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		panelFontHeight.add(this.labelMinFontHeight,     new GridBagConstraintsHelper(0, 1, 2, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		this.textFieldMinFontHeight.setText("0");
		panelFontHeight.add(this.textFieldMinFontHeight, new GridBagConstraintsHelper(2, 1, 1, 1).setAnchor(GridBagConstraints.EAST).setWeight(1, 0).setInsets(2,10,2,10).setIpad(50, 0));
		panelFontHeight.add(this.labelMinFontHeightUnity,new GridBagConstraintsHelper(3, 1, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		//@formatter:on
	}

	private void initPanelTextExtentInflation(JPanel panelTextExtentInflation) {
		//@formatter:off
		panelTextExtentInflation.setLayout(new GridBagLayout());
		panelTextExtentInflation.add(this.labelHorizontal,      new GridBagConstraintsHelper(0, 0, 2, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		this.textFieldHorizontal.setText("0");
		panelTextExtentInflation.add(this.textFieldHorizontal,  new GridBagConstraintsHelper(2, 0, 1, 1).setAnchor(GridBagConstraints.EAST).setWeight(1, 0).setInsets(2,10,2,10).setIpad(50, 0));
		panelTextExtentInflation.add(this.labelHorizontalUnity, new GridBagConstraintsHelper(3, 0, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		panelTextExtentInflation.add(this.labelVertical,        new GridBagConstraintsHelper(0, 1, 2, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		this.textFieldVertical.setText("0");
		panelTextExtentInflation.add(this.textFieldVertical,    new GridBagConstraintsHelper(2, 1, 1, 1).setAnchor(GridBagConstraints.EAST).setWeight(1, 0).setInsets(2,10,2,10).setIpad(50, 0));
		panelTextExtentInflation.add(this.labelVerticalUnity,   new GridBagConstraintsHelper(3, 1, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		//@formatter:on
	}

	private void initPanelFontWidth(JPanel panelFontWide) {
		//@formatter:off
		panelFontWide.setLayout(new GridBagLayout());
		panelFontWide.add(this.labelMaxFontWidth,      new GridBagConstraintsHelper(0, 0, 2, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		textFieldMaxFontWidth.setText("0");
		panelFontWide.add(this.textFieldMaxFontWidth,  new GridBagConstraintsHelper(2, 0, 1, 1).setAnchor(GridBagConstraints.EAST).setWeight(1, 0).setInsets(2,10,2,10).setIpad(50, 0));
		panelFontWide.add(this.labelMaxFontWidthUnity, new GridBagConstraintsHelper(3, 0, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		panelFontWide.add(this.labelMinFontWidth,      new GridBagConstraintsHelper(0, 1, 2, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		textFieldMinFontWidth.setText("0");
		panelFontWide.add(this.textFieldMinFontWidth,  new GridBagConstraintsHelper(2, 1, 1, 1).setAnchor(GridBagConstraints.EAST).setWeight(1, 0).setInsets(2,10,2,10).setIpad(50, 0));
		panelFontWide.add(this.labelMinFontWidthUnity, new GridBagConstraintsHelper(3, 1, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		//@formatter:on
	}

	private void initPanelRotateLabel(JPanel panelRotateLabel) {
		//@formatter:off
		panelRotateLabel.setLayout(new GridBagLayout());
		panelRotateLabel.add(this.checkBoxFixedFontAngl,      new GridBagConstraintsHelper(0, 0, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		panelRotateLabel.add(this.checkBoxRemoveRepeatLabel,   new GridBagConstraintsHelper(1, 0, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		panelRotateLabel.add(this.labelLineDirection,         new GridBagConstraintsHelper(0, 1, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		comboBoxLineDirection.setEditable(true);
		panelRotateLabel.add(this.comboBoxLineDirection,      new GridBagConstraintsHelper(1, 1, 1, 1).setAnchor(GridBagConstraints.CENTER).setWeight(1, 0).setInsets(2,10,2,10).setIpad(30, 0));
		panelRotateLabel.add(this.labelFontSpace,             new GridBagConstraintsHelper(0, 2, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));

		panelRotateLabel.add(this.spinnerFontSpace,           new GridBagConstraintsHelper(1, 2, 1, 1).setAnchor(GridBagConstraints.CENTER).setWeight(1, 0).setInsets(2,10,2,10).setIpad(30, 0));
		panelRotateLabel.add(this.labelRepeatInterval,        new GridBagConstraintsHelper(0, 3, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		textFieldRepeatInterval.setText("0");
		panelRotateLabel.add(this.textFieldRepeatInterval,    new GridBagConstraintsHelper(1, 3, 1, 1).setAnchor(GridBagConstraints.CENTER).setWeight(1, 0).setInsets(2,10,2,10).setIpad(60, 0));
		panelRotateLabel.add(this.labelWordDisplay,           new GridBagConstraintsHelper(0, 4, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		comboBoxWordDisplay.setEditable(true);
		panelRotateLabel.add(this.comboBoxWordDisplay,        new GridBagConstraintsHelper(1, 4, 1, 1).setAnchor(GridBagConstraints.CENTER).setWeight(1, 0).setInsets(2,10,2,10).setIpad(30, 0));		
		panelRotateLabel.add(this.checkBoxRepeatIntervalFixed,new GridBagConstraintsHelper(0, 5, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		//@formatter:on
	}

	private void initPanelTextFontSet(JPanel panelTextFontSet) {
		//@formatter:off
		panelTextFontSet.setLayout(new GridBagLayout());
		panelTextFontSet.add(this.labelOverLength,      new GridBagConstraintsHelper(0, 0, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		comboBoxOverLength.setEditable(true);
		panelTextFontSet.add(this.comboBoxOverLength,   new GridBagConstraintsHelper(1, 0, 1, 1).setAnchor(GridBagConstraints.CENTER).setWeight(1, 0).setInsets(2,10,2,10).setIpad(30, 0));
		panelTextFontSet.add(this.labelFontCount,       new GridBagConstraintsHelper(0, 1, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		panelTextFontSet.add(this.spinnerFontCount,     new GridBagConstraintsHelper(1, 1, 1, 1).setAnchor(GridBagConstraints.CENTER).setWeight(1, 0).setInsets(2,10,2,10).setIpad(30, 0));
		//@formatter:on
	}

}
