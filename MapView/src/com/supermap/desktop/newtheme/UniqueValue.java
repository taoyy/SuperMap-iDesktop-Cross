package com.supermap.desktop.newtheme;
import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * 包含右Panel的类,用于table渲染
 * 
 * @author xie
 * 
 */
public class UniqueValue extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel label;
	private Color color;

	public UniqueValue() {
		super();
		this.setLayout(new GridLayout(1, 1));
		add(getLabel());
	}

	/**
	 * 颜色Label
	 * 
	 * @return
	 */
	public JLabel getLabel() {
		if (label == null) {
			label = new JLabel();
		}
		return label;
	}

	/**
	 * 返回內部panel的顏色
	 * 
	 * @return m_color
	 */
	public Color getColor() {
		color = label.getBackground();
		return color;
	}

	/**
	 * 设置颜色
	 * 
	 * @param color
	 */
	public void setColor(Color color) {
		label.setBackground(color);

	}

}
