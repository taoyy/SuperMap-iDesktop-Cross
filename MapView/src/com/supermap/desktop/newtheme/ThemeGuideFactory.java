package com.supermap.desktop.newtheme;

import java.awt.GridBagConstraints;

import com.supermap.data.DatasetVector;
import com.supermap.desktop.Application;
import com.supermap.desktop.Interface.IDockbar;
import com.supermap.desktop.ui.controls.GridBagConstraintsHelper;

public class ThemeGuideFactory {

	private static final String THEME_MAIN_CONTAINER_CLASS = "com.supermap.desktop.newtheme.ThemeMainContainer";

	public static void buildUniqueTheme() {
		try {
			IDockbar dockbarPropertyContainer = Application.getActiveApplication().getMainFrame().getDockbarManager()
					.get(Class.forName(THEME_MAIN_CONTAINER_CLASS));

			if (dockbarPropertyContainer != null) {
				ThemeMainContainer container = (ThemeMainContainer) dockbarPropertyContainer.getComponent();
				container.remove(container.getPanelThemeInfo());
				DatasetVector dataset = (DatasetVector) Application.getActiveApplication().getActiveDatasets()[0];
				container.add(new ThemeUniqueContainer(dataset),new GridBagConstraintsHelper(0, 1, 2, 1).setWeight(3, 3).setInsets(5).setAnchor(GridBagConstraints.CENTER).setIpad(0, 0).setFill(GridBagConstraints.BOTH));
				dockbarPropertyContainer.setVisible(true);
			}
		} catch (ClassNotFoundException e) {
			Application.getActiveApplication().getOutput().output(e);
		}
	}

	public static void buildRangeTheme() {
		try {
			IDockbar dockbarPropertyContainer = Application.getActiveApplication().getMainFrame().getDockbarManager()
					.get(Class.forName(THEME_MAIN_CONTAINER_CLASS));

			if (dockbarPropertyContainer != null) {
				ThemeMainContainer container = (ThemeMainContainer) dockbarPropertyContainer.getComponent();
				container.remove(container.getPanelThemeInfo());
				DatasetVector dataset = (DatasetVector) Application.getActiveApplication().getActiveDatasets()[0];
				container.add(new ThemeRangeContainer(dataset),new GridBagConstraintsHelper(0, 1, 2, 1).setWeight(3, 3).setInsets(5).setAnchor(GridBagConstraints.CENTER).setIpad(0, 0).setFill(GridBagConstraints.BOTH));
				dockbarPropertyContainer.setVisible(true);
			}
		} catch (ClassNotFoundException e) {
			Application.getActiveApplication().getOutput().output(e);
		}
	}
}
