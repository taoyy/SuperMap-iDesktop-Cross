package com.supermap.desktop.newtheme;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.text.MessageFormat;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import com.supermap.data.ColorGradientType;
import com.supermap.data.Colors;
import com.supermap.data.Dataset;
import com.supermap.data.DatasetType;
import com.supermap.data.DatasetVector;
import com.supermap.data.FieldInfo;
import com.supermap.data.FieldType;
import com.supermap.data.GeoStyle;
import com.supermap.data.Resources;
import com.supermap.data.SymbolType;
import com.supermap.desktop.Application;
import com.supermap.desktop.CommonToolkit;
import com.supermap.desktop.Interface.IFormMap;
import com.supermap.desktop.mapview.MapViewProperties;
import com.supermap.desktop.ui.UICommonToolkit;
import com.supermap.desktop.ui.controls.ColorsComboBox;
import com.supermap.desktop.ui.controls.DialogResult;
import com.supermap.desktop.ui.controls.GridBagConstraintsHelper;
import com.supermap.desktop.ui.controls.InternalImageIconFactory;
import com.supermap.desktop.ui.controls.SQLExpressionDialog;
import com.supermap.desktop.ui.controls.SymbolDialog;
import com.supermap.mapping.Layer;
import com.supermap.mapping.Map;
import com.supermap.mapping.RangeMode;
import com.supermap.mapping.ThemeRange;
import com.supermap.mapping.ThemeRangeItem;
import com.supermap.ui.MapControl;

import javax.swing.DefaultComboBoxModel;

public class ThemeRangeContainer extends JPanel {

	private static final long serialVersionUID = 1L;
	private static final int TABLE_COLUMN_VISIBLE = 0;
	private static final int TABLE_COLUMN_GEOSTYLE = 1;
	private static final int TABLE_COLUMN_CAPTION = 2;
	private JTabbedPane tabbedPaneInfo = new JTabbedPane();
	private JPanel panelProperty = new JPanel();
	private JLabel labelExpression = new JLabel();
	private JComboBox<String> comboBoxExpression = new JComboBox<String>();
	private JLabel labelRangeMethod = new JLabel();
	private JComboBox<String> comboBoxRangeMethod = new JComboBox<String>();
	private JLabel labelRangeCount = new JLabel();
	private JComboBox<String> comboBoxRangeCount = new JComboBox<String>();
	private JLabel labelRangeLength = new JLabel();
	private JSpinner spinnerRangeLength = new JSpinner();
	private JLabel labelRangePrecision = new JLabel();
	private JComboBox<String> comboBoxRangePrecision = new JComboBox<String>();
	private JLabel labelRangeFormat = new JLabel();
	private JComboBox<String> comboBoxRangeFormat = new JComboBox<String>();
	private JLabel labelColorStyle = new JLabel();
	private ColorsComboBox comboBoxColorStyle = new ColorsComboBox();
	private JToolBar toolBar = new JToolBar();
	private JButton buttonMerge = new JButton();
	private JButton buttonSplit = new JButton();
	private JButton buttonVisible = new JButton();
	private JButton buttonStyle = new JButton();
	private JLabel labelRangeValue = new JLabel();
	private JTextField textFieldRangeValue = new JTextField();
	private JScrollPane scrollPane = new JScrollPane();
	private JTable tableRangeInfo = new JTable();
	private static String[] nameStrings = { MapViewProperties.getString("String_Title_Visible"), MapViewProperties.getString("String_Title_Sytle"),
			MapViewProperties.getString("String_ThemeGraphItemManager_ClmExpression") };
	private DatasetVector datasetVector;
	private Map map;
	private ThemeRange themeRange;
	private Layer themeRangeLayer;
	private String numeric = "#";
	private LocalActionListener actionListener = new LocalActionListener();
	private LocalTableMouseListener tableMouseListener = new LocalTableMouseListener();
	private LocalComboBoxItemListener itemListener = new LocalComboBoxItemListener();
	private String rangeExpression = "SimID";
	private RangeMode rangeMode = RangeMode.EQUALINTERVAL;
	private int rangeCount = 2;
	private String captiontype = "<=X<";

	public ThemeRangeContainer(DatasetVector datasetVector) {
		this.datasetVector = datasetVector;
		this.map = initCurrentTheme(datasetVector);
		initComponents();
		initResources();
		registActionListener();
	}

	/**
	 * 初始化单值专题图
	 * 
	 * @param dataset
	 * @return
	 */
	private Map initCurrentTheme(DatasetVector dataset) {
		this.themeRange = ThemeRange.makeDefault(dataset, "SmID", RangeMode.EQUALINTERVAL, 5, ColorGradientType.GREENRED);
		IFormMap formMap = (IFormMap) Application.getActiveApplication().getActiveForm();
		MapControl mapControl = null;
		if (null != formMap.getMapControl()) {
			mapControl = formMap.getMapControl();
			this.themeRangeLayer = mapControl.getMap().getLayers().add(dataset, themeRange, true);
			this.themeRange = (ThemeRange) themeRangeLayer.getTheme();
			mapControl.getMap().refresh();
		}
		return mapControl.getMap();
	}

	/**
	 * 界面布局入口
	 */
	private void initComponents() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		this.setLayout(gridBagLayout);
		this.tabbedPaneInfo.add(MapViewProperties.getString("String_Theme_Property"), this.panelProperty);
		this.add(tabbedPaneInfo, new GridBagConstraintsHelper(0, 0, 1, 1).setAnchor(GridBagConstraints.CENTER).setFill(GridBagConstraints.BOTH).setWeight(1, 1));
		initPanelProperty();
	}

	/**
	 * 初始化属性界面
	 */
	private void initPanelProperty() {
		//@formatter:off
		initToolBar();
		getFieldComboBox(comboBoxExpression);
		initComboBoxRangMethod();
		initComboBoxRangeCount();
		initComboBoxRangePrecision();
		initComboBoxRangeFormat();
		textFieldRangeValue.setText(new DecimalFormat(numeric).format(themeRange.getItem(0).getEnd()));
		this.panelProperty.setLayout(new GridBagLayout());
		this.panelProperty.add(this.labelExpression,     new GridBagConstraintsHelper(0, 0, 1, 1).setAnchor(GridBagConstraints.WEST).setInsets(2,10,2,10).setWeight(3, 0));
		comboBoxExpression.setEditable(true);
		this.panelProperty.add(this.comboBoxExpression,  new GridBagConstraintsHelper(1, 0, 1, 1).setAnchor(GridBagConstraints.WEST).setInsets(2).setWeight(3, 0).setIpad(30, 0));
		this.panelProperty.add(this.labelRangeMethod,    new GridBagConstraintsHelper(0, 1, 1, 1).setAnchor(GridBagConstraints.WEST).setInsets(2,10,2,10).setWeight(3, 0));
		this.panelProperty.add(this.comboBoxRangeMethod, new GridBagConstraintsHelper(1, 1, 1, 1).setAnchor(GridBagConstraints.WEST).setInsets(2).setWeight(3, 0).setIpad(150, 0));
		this.panelProperty.add(this.labelRangeCount,     new GridBagConstraintsHelper(0, 2, 1, 1).setAnchor(GridBagConstraints.WEST).setInsets(2,10,2,10).setWeight(3, 0));
		this.panelProperty.add(this.comboBoxRangeCount,  new GridBagConstraintsHelper(1, 2, 1, 1).setAnchor(GridBagConstraints.WEST).setInsets(2).setWeight(3, 0).setIpad(155, 0));
		this.panelProperty.add(this.labelRangeLength,    new GridBagConstraintsHelper(0, 3, 1, 1).setAnchor(GridBagConstraints.WEST).setInsets(2,10,2,10).setWeight(3, 0));
		this.spinnerRangeLength.setEnabled(false);
		this.panelProperty.add(this.spinnerRangeLength,  new GridBagConstraintsHelper(1, 3, 1, 1).setAnchor(GridBagConstraints.WEST).setInsets(2).setWeight(3, 0).setIpad(205, 0));
		this.panelProperty.add(this.labelRangePrecision,   new GridBagConstraintsHelper(0, 4, 1, 1).setAnchor(GridBagConstraints.WEST).setInsets(2,10,2,10).setWeight(3, 0));
		this.panelProperty.add(this.comboBoxRangePrecision,new GridBagConstraintsHelper(1, 4, 1, 1).setAnchor(GridBagConstraints.WEST).setInsets(2).setWeight(3, 0).setIpad(155, 0));
		this.panelProperty.add(this.labelRangeFormat,      new GridBagConstraintsHelper(0, 5, 1, 1).setAnchor(GridBagConstraints.WEST).setInsets(2,10,2,10).setWeight(3, 0));
		this.panelProperty.add(this.comboBoxRangeFormat,   new GridBagConstraintsHelper(1, 5, 1, 1).setAnchor(GridBagConstraints.WEST).setInsets(2).setWeight(3, 0).setIpad(155, 0));
		this.panelProperty.add(this.labelColorStyle,       new GridBagConstraintsHelper(0, 6, 1, 1).setAnchor(GridBagConstraints.WEST).setInsets(2,10,2,10).setWeight(3, 0));
		this.panelProperty.add(this.comboBoxColorStyle,    new GridBagConstraintsHelper(1, 6, 1, 1).setAnchor(GridBagConstraints.WEST).setInsets(2).setWeight(3, 0).setIpad(55, 0));
		this.panelProperty.add(this.toolBar,               new GridBagConstraintsHelper(0, 7, 2, 1).setAnchor(GridBagConstraints.WEST).setInsets(2).setWeight(3, 0));
		this.panelProperty.add(this.labelRangeValue,       new GridBagConstraintsHelper(0, 8, 1, 1).setAnchor(GridBagConstraints.WEST).setInsets(2).setWeight(3, 0));
		this.textFieldRangeValue.setText("0");
		this.panelProperty.add(this.textFieldRangeValue,   new GridBagConstraintsHelper(1, 8, 1, 1).setAnchor(GridBagConstraints.WEST).setInsets(2).setWeight(3, 0).setIpad(235, 0));
		this.panelProperty.add(this.scrollPane,            new GridBagConstraintsHelper(0, 9, 2, 1).setAnchor(GridBagConstraints.NORTH).setInsets(2).setWeight(1, 3).setFill(GridBagConstraints.BOTH));
		getTable();
		this.scrollPane.setViewportView(tableRangeInfo);
		//@formatter:on
	}

	/**
	 * 初始化分段方法项
	 */
	private void initComboBoxRangMethod() {
		comboBoxRangeMethod.setModel(new DefaultComboBoxModel<String>(new String[] { MapViewProperties.getString("String_RangeMode_EqualInterval"),
				MapViewProperties.getString("String_RangeMode_SquareRoot"),
				MapViewProperties.getString("String_RangeMode_StdDeviation"),
				MapViewProperties.getString("String_RangeMode_Logarithm"),
				MapViewProperties.getString("String_RangeMode_Quantile"),
				MapViewProperties.getString("String_RangeMode_CustomInterval") }));
		comboBoxRangeMethod.setEditable(true);
	}

	/**
	 * 初始化段数
	 */
	private void initComboBoxRangeCount() {
		comboBoxRangeCount.setModel(new DefaultComboBoxModel<String>(new String[] { "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15",
				"16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32" }));
		comboBoxRangeCount.setEditable(true);
		int rangeCount = themeRange.getCount();
		comboBoxRangeCount.setSelectedIndex(rangeCount - 2);
	}

	/**
	 * 初始化段值精度项
	 */
	private void initComboBoxRangePrecision() {
		comboBoxRangePrecision
				.setModel(new DefaultComboBoxModel<String>(new String[] { "1", "0.1", "0.01", "0.001", "0.0001", "0.00001", "0.000001", "0.0000001" }));
		comboBoxRangePrecision.setSelectedIndex(0);
		comboBoxRangePrecision.setEditable(true);
	}

	private void initComboBoxRangeFormat() {
		comboBoxRangeFormat.setModel(new DefaultComboBoxModel<String>(new String[] { "0-100", "0<=x<100" }));
		comboBoxRangeFormat.setSelectedIndex(1);
		comboBoxRangeFormat.setEditable(true);
	}

	/*
	 * 资源化
	 */
	private void initResources() {
		this.labelExpression.setText(MapViewProperties.getString("String_label_Expression"));
		this.labelRangeMethod.setText(MapViewProperties.getString("String_Label_RangeMethed"));
		this.labelRangeCount.setText(MapViewProperties.getString("String_Label_RangeCount"));
		this.labelRangeLength.setText(MapViewProperties.getString("String_Label_RangeSize"));
		this.labelRangePrecision.setText(MapViewProperties.getString("String_RangePrecision"));
		this.labelRangeFormat.setText(MapViewProperties.getString("String_Label_CaptionFormat"));
		this.labelColorStyle.setText(MapViewProperties.getString("String_Label_ColorScheme"));
		this.labelRangeValue.setText(MapViewProperties.getString("String_Label_RangeValue"));
		buttonMerge.setEnabled(false);
		this.buttonMerge.setToolTipText(MapViewProperties.getString("String_Title_Merge"));
		buttonSplit.setEnabled(false);
		this.buttonSplit.setToolTipText(MapViewProperties.getString("String_Title_Split"));
		this.buttonStyle.setToolTipText(MapViewProperties.getString("String_Title_Sytle"));
		this.buttonVisible.setToolTipText(MapViewProperties.getString("String_Title_Visible"));
	}

	/**
	 * 表格初始化
	 * 
	 * @return m_table
	 */
	private JTable getTable() {
		DefaultTableModel defaultTableModel = new DefaultTableModel(getData(), nameStrings);
		this.tableRangeInfo = new JTable(defaultTableModel);
		this.tableRangeInfo.getTableHeader().setReorderingAllowed(false);
		this.tableRangeInfo.setRowSelectionAllowed(true);
		this.tableRangeInfo.setColumnSelectionAllowed(true);
		this.tableRangeInfo.setRowHeight(25);
		initTableRender();
		return this.tableRangeInfo;
	}

	/**
	 * 获取表格需要的数据
	 * 
	 * @return tableData
	 */
	private Object[][] getData() {
		int rangeCount = this.themeRange.getCount();
		if (rangeCount == 0) {
			return new Object[0][3];
		}
		Object[][] tableData = new Object[rangeCount][3];
		for (int i = 0; i < rangeCount; i++) {
			ThemeRangeItem rangeItem = themeRange.getItem(i);
			GeoStyle geoStyle = rangeItem.getStyle();
			UniqueValue uniqueValue = new UniqueValue();
			DatasetType datasetType = datasetVector.getType();
			if (CommonToolkit.DatasetTypeWrap.isPoint(datasetType) || CommonToolkit.DatasetTypeWrap.isLine(datasetType)) {
				uniqueValue.setColor(geoStyle.getLineColor());
			} else if (CommonToolkit.DatasetTypeWrap.isRegion(datasetType)) {
				uniqueValue.setColor(geoStyle.getFillForeColor());
			}
			VisibleValue visibleValue = new VisibleValue(rangeItem);
			tableData[i][TABLE_COLUMN_VISIBLE] = visibleValue;
			tableData[i][TABLE_COLUMN_GEOSTYLE] = uniqueValue;
			String start = new DecimalFormat(numeric).format(rangeItem.getStart());
			String end = new DecimalFormat(numeric).format(rangeItem.getEnd());
			if (captiontype.contains("<")) {
				if (0 == i) {
					tableData[i][TABLE_COLUMN_CAPTION] = "min<X<" + end;
				} else if (rangeCount - 1 == i) {
					tableData[i][TABLE_COLUMN_CAPTION] = start + "<=X<max";
				} else {
					tableData[i][TABLE_COLUMN_CAPTION] = start + "<=X<" + end;
				}
			} else {
				if (0 == i) {
					tableData[i][TABLE_COLUMN_CAPTION] = "min-" + end;
				} else if (rangeCount - 1 == i) {
					tableData[i][TABLE_COLUMN_CAPTION] = start + "-max";
				} else {
					tableData[i][TABLE_COLUMN_CAPTION] = start + "-" + end;
				}
			}

		}

		return tableData;
	}

	/**
	 * table渲染和颜色控件事件修改
	 */
	private void initTableRender() {
		TableColumn visibleColumn = tableRangeInfo.getColumn(MapViewProperties.getString("String_Title_Visible"));
		TableColumn viewColumn = tableRangeInfo.getColumn(MapViewProperties.getString("String_Title_Sytle"));
		JPanelCellEditorGeoStyle cellEditor = new JPanelCellEditorGeoStyle(datasetVector);
		JPanelRendererGeoStyle cellRenderer = new JPanelRendererGeoStyle();
		cellEditor.addPropertyChangeListener("Color",
				new PropertyChangeListener() {
					public void propertyChange(PropertyChangeEvent evt) {
						int index = tableRangeInfo.getSelectedRow();
						GeoStyle textStyle = (GeoStyle) evt.getNewValue();
						((ThemeRange) themeRangeLayer.getTheme()).getItem(index).setStyle(textStyle);
						map.refresh();
						tableRangeInfo.repaint();
					}
				});
		JPanelCellEditorVisible visibleEditor = new JPanelCellEditorVisible(JPanelCellEditorVisible.THEME_RANGE_TYPE);
		JPanelRenderVisible visibleRender = new JPanelRenderVisible();
		visibleEditor.addPropertyChangeListener("Visible",
				new PropertyChangeListener() {
					public void propertyChange(PropertyChangeEvent evt) {
						int index = tableRangeInfo.getSelectedRow();
						boolean visible = (boolean) evt.getNewValue();
						((ThemeRange) themeRangeLayer.getTheme()).getItem(index).setVisible(visible);
						map.refresh();
						tableRangeInfo.repaint();
					}
				});
		visibleColumn.setCellEditor(visibleEditor);
		visibleColumn.setCellRenderer(visibleRender);
		viewColumn.setCellRenderer(cellRenderer);
		viewColumn.setCellEditor(cellEditor);
	}

	/**
	 * 表达式
	 * 
	 * @return m_fieldComboBox
	 */
	private JComboBox<String> getFieldComboBox(JComboBox<String> comboBox) {
		int count = datasetVector.getFieldCount();
		for (int j = 0; j < count; j++) {
			FieldInfo fieldInfo = datasetVector.getFieldInfos().get(j);
			if (fieldInfo.getType() != FieldType.TEXT) {
				String item = datasetVector.getName() + "." + fieldInfo.getName();
				comboBox.addItem(item);
			}
		}
		comboBox.addItem(MapViewProperties.getString("String_Combobox_Expression"));
		return comboBox;
	}

	/**
	 * 颜色方案改变时刷新颜色
	 */
	private void refreshColor() {
		if (comboBoxColorStyle != null) {
			int colorCount = ((Colors) comboBoxColorStyle.getSelectedItem())
					.getCount();
			Colors colors = (Colors) comboBoxColorStyle.getSelectedItem();
			int rangeCount = themeRange.getCount();
			if (rangeCount > 0) {
				float ratio = (1f * colorCount) / (1f * rangeCount);
				setGeoStyleColor(themeRange.getItem(0).getStyle(), colors.get(0));
				setGeoStyleColor(themeRange.getItem(rangeCount - 1).getStyle(), colors.get(colorCount - 1));
				for (int i = 1; i < rangeCount - 1; i++) {
					int colorIndex = Math.round(i * ratio);
					if (colorIndex == colorCount) {
						colorIndex--;
					}
					setGeoStyleColor(themeRange.getItem(i).getStyle(), colors.get(colorIndex));
				}
			}
		}
	}

	/**
	 * 根据当前数据集类型设置颜色方案
	 * 
	 * @param geoStyle 需要设置的风格
	 * @param color 设置的颜色
	 */
	private void setGeoStyleColor(GeoStyle geoStyle, Color color) {
		DatasetType datasetType = datasetVector.getType();
		if (CommonToolkit.DatasetTypeWrap.isPoint(datasetType) || CommonToolkit.DatasetTypeWrap.isLine(datasetType)) {
			geoStyle.setLineColor(color);
		} else if (CommonToolkit.DatasetTypeWrap.isRegion(datasetType)) {
			geoStyle.setFillForeColor(color);
		}
	}

	/**
	 * 初始化工具条
	 */
	private void initToolBar() {
		this.toolBar.add(this.buttonMerge);
		this.toolBar.add(this.buttonSplit);
		this.toolBar.addSeparator();
		this.toolBar.add(this.buttonVisible);
		this.toolBar.add(this.buttonStyle);
		this.toolBar.addSeparator();
		this.buttonMerge.setIcon(InternalImageIconFactory.Merge);
		this.buttonSplit.setIcon(InternalImageIconFactory.Split);
		this.buttonStyle.setIcon(InternalImageIconFactory.STYLE);
		this.buttonVisible.setIcon(InternalImageIconFactory.VISIBLE);
	}

	private void registActionListener() {
		this.buttonVisible.addActionListener(this.actionListener);
		this.buttonStyle.addActionListener(this.actionListener);
		this.buttonMerge.addActionListener(this.actionListener);
		this.buttonSplit.addActionListener(this.actionListener);
		this.tableRangeInfo.addMouseListener(this.tableMouseListener);
		this.comboBoxColorStyle.addItemListener(this.itemListener);
		this.comboBoxExpression.addItemListener(this.itemListener);
		this.comboBoxRangePrecision.addItemListener(this.itemListener);
		this.comboBoxRangeCount.addItemListener(this.itemListener);
		this.comboBoxRangeMethod.addItemListener(this.itemListener);
		this.comboBoxRangeFormat.addItemListener(this.itemListener);
	}

	private void unregistActionListener() {

	}

	/**
	 * 刷新table
	 */
	private void refreshTable() {
		DefaultTableModel defaultTableModel = new DefaultTableModel(getData(), nameStrings);
		tableRangeInfo.setModel(defaultTableModel);
		initTableRender();
		tabbedPaneInfo.repaint();
	}

	class LocalActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == buttonMerge) {
				int[] selectedRows = tableRangeInfo.getSelectedRows();
				if (selectedRows.length == tableRangeInfo.getRowCount()) {
					UICommonToolkit.showConfirmDialog(MapViewProperties.getString("String_Warning_RquiredTwoFieldForRange"));
				} else {
					// 合并选中项
					mergeItem();
				}
			} else if (e.getSource() == buttonSplit) {
				// 拆分选中项
				splitItem();
			} else if (e.getSource() == buttonVisible) {
				// 批量修改分段的可见状态
				setItemVisble();
			} else if (e.getSource() == buttonStyle) {
				// 批量修改单值段的符号方案
				setItemGeoSytle();
			}
		}

		private void splitItem() {
			int[] selectRow = tableRangeInfo.getSelectedRows();
			ThemeRangeItem item = themeRange.getItem(selectRow[0]);
			double splitValue = (item.getEnd() + item.getStart()) / 2;
			String startCaption = MessageFormat.format(MapViewProperties.getString("String_RangeFormat"), String.valueOf(item.getStart()),
					String.valueOf(splitValue));
			String endCaption = MessageFormat.format(MapViewProperties.getString("String_RangeFormat"), String.valueOf(splitValue),
					String.valueOf(item.getEnd()));
			themeRange.split(selectRow[0], splitValue, item.getStyle(), startCaption, item.getStyle(), endCaption);
			map.refresh();
			refreshTable();
			UICommonToolkit.getLayersManager().getLayersTree().reload();
		}

		private void mergeItem() {
			int[] selectedRows = tableRangeInfo.getSelectedRows();
			int startIndex = selectedRows[0];
			int endIndex = selectedRows[selectedRows.length - 1];
			ThemeRangeItem startItem = themeRange.getItem(startIndex);
			ThemeRangeItem endItem = themeRange.getItem(endIndex);
			String caption = MessageFormat.format(MapViewProperties.getString("String_RangeFormat"), String.valueOf(startItem.getStart()),
					String.valueOf(endItem.getEnd()));
			themeRange.merge(startIndex, endIndex, startItem.getStyle(), caption);
			map.refresh();
			refreshTable();
			UICommonToolkit.getLayersManager().getLayersTree().reload();
		}

		/**
		 * 设置分段项是否可见
		 */
		private void setItemVisble() {
			int[] selectedRow = tableRangeInfo.getSelectedRows();
			if (selectedRow.length == 1) {
				resetVisible(selectedRow[0]);
			} else {
				for (int i = 0; i < selectedRow.length; i++) {
					resetVisible(selectedRow[i]);
				}
			}
			map.refresh();
			refreshTable();
			UICommonToolkit.getLayersManager().getLayersTree().reload();
		}

		/**
		 * 重置可见选项
		 * 
		 * @param selectRow 要重置的行
		 */
		private void resetVisible(int selectRow) {
			ThemeRangeItem tempThemeRangeItem = ((ThemeRange) themeRangeLayer.getTheme()).getItem(selectRow);
			boolean visible = tempThemeRangeItem.isVisible();
			if (visible) {
				tempThemeRangeItem.setVisible(false);
				((VisibleValue) tableRangeInfo.getValueAt(selectRow, 0)).setIcon(InternalImageIconFactory.INVISIBLE);
			} else {
				tempThemeRangeItem.setVisible(true);
				((VisibleValue) tableRangeInfo.getValueAt(selectRow, 0)).setIcon(InternalImageIconFactory.VISIBLE);
			}
		}

		/**
		 * 批量设置文本风格
		 */
		private void setItemGeoSytle() {
			int[] selectedRow = tableRangeInfo.getSelectedRows();
			SymbolDialog textStyleDialog = new SymbolDialog();
			String name = tableRangeInfo.getColumnName(TABLE_COLUMN_VISIBLE);
			int width = tableRangeInfo.getColumn(name).getWidth();
			int height = tableRangeInfo.getTableHeader().getHeight();
			int x = tableRangeInfo.getLocationOnScreen().x + width;
			int y = tableRangeInfo.getLocationOnScreen().y - height;
			textStyleDialog.setLocation(x, y);
			Resources resources = Application.getActiveApplication().getWorkspace().getResources();
			SymbolType symbolType = null;
			GeoStyle geoStyle = new GeoStyle();
			if (CommonToolkit.DatasetTypeWrap.isPoint(datasetVector.getType())) {
				symbolType = SymbolType.MARKER;
			} else if (CommonToolkit.DatasetTypeWrap.isLine(datasetVector.getType())) {
				symbolType = SymbolType.LINE;
			} else if (CommonToolkit.DatasetTypeWrap.isRegion(datasetVector.getType())) {
				symbolType = SymbolType.FILL;
			}
			DialogResult dialogResult = textStyleDialog.showDialog(resources, geoStyle, symbolType);
			if (dialogResult.equals(DialogResult.OK)) {
				GeoStyle nowGeoStyle = textStyleDialog.getStyle();
				if (selectedRow.length == 1) {
					resetGeoSytle(selectedRow[0], nowGeoStyle, symbolType);
				} else {
					for (int i = 0; i < selectedRow.length; i++) {
						resetGeoSytle(selectedRow[i], nowGeoStyle, symbolType);
					}
				}
			}
			map.refresh();
			refreshTable();
			UICommonToolkit.getLayersManager().getLayersTree().reload();
		}

		/**
		 * 重置文本风格
		 * 
		 * @param selectRow 要重置文本风格的行
		 * @param nowGeoStyle 新的文本风格
		 * @param symbolType 文本的风格类型
		 */
		private void resetGeoSytle(int selectRow, GeoStyle nowGeoStyle, SymbolType symbolType) {
			ThemeRangeItem item = ((ThemeRange) themeRangeLayer.getTheme()).getItem(selectRow);
			item.setStyle(nowGeoStyle);
			UniqueValue nowValue = (UniqueValue) tableRangeInfo.getValueAt(selectRow, 1);
			if (symbolType == SymbolType.MARKER || symbolType == SymbolType.LINE) {
				nowValue.setColor(item.getStyle().getLineColor());
			} else {
				nowValue.setColor(item.getStyle().getFillForeColor());
			}
		}
	}

	class LocalTableMouseListener extends MouseAdapter {
		@Override
		public void mouseReleased(MouseEvent e) {
			int[] selectedRows = tableRangeInfo.getSelectedRows();
			if (selectedRows.length == 1) {
				buttonMerge.setEnabled(false);
				buttonSplit.setEnabled(true);
			} else if (selectedRows.length >= 2) {
				buttonMerge.setEnabled(true);
				buttonSplit.setEnabled(false);
			}
		}
	}

	class LocalComboBoxItemListener implements ItemListener {

		@Override
		public void itemStateChanged(ItemEvent e) {
			if (e.getStateChange() == ItemEvent.SELECTED) {
				if (e.getSource() == comboBoxColorStyle) {
					// 修改颜色方案
					refreshColor();
					UICommonToolkit.getLayersManager().getLayersTree().reload();
					map.refresh();
					refreshTable();
				} else if (e.getSource() == comboBoxExpression) {
					// sql表达式
					getSqlExpression(comboBoxExpression);
					// 修改表达式
					setFieldInfo();
				} else if (e.getSource() == comboBoxRangeCount) {
					// 修改段数
					setRangeCount();
				} else if (e.getSource() == comboBoxRangePrecision) {
					// 设置分段舍入精度
					setRangePrecision();
				} else if (e.getSource() == comboBoxRangeMethod) {
					// 设置分段方法
					setRangeMethod();
				} else if (e.getSource() == comboBoxRangeFormat) {
					// 设置标题格式
					setRangeFormat();
				}
			}
		}

		/**
		 * 设置标题格式
		 */
		private void setRangeFormat() {
			int count = comboBoxRangeFormat.getSelectedIndex();
			if (0 == count) {
				captiontype = "-";
			} else {
				captiontype = "<=x<";
			}
			refreshTable();
		}

		private void setRangeCount() {
			rangeCount = Integer.valueOf(comboBoxRangeCount.getSelectedItem().toString());
			resetThemeInfo();
		}

		/**
		 * 设置分段方法
		 */
		private void setRangeMethod() {
			String rangeMethod = comboBoxRangeMethod.getSelectedItem().toString();
			if (rangeMethod.equals(MapViewProperties.getString("String_RangeMode_EqualInterval"))) {
				// 等距分段
				rangeMode = RangeMode.EQUALINTERVAL;
				comboBoxRangeCount.setEnabled(true);
				spinnerRangeLength.setEnabled(false);
			} else if (rangeMethod.equals(MapViewProperties.getString("String_RangeMode_SquareRoot"))) {
				// 平方根分度
				rangeMode = RangeMode.SQUAREROOT;
				comboBoxRangeCount.setEnabled(true);
				spinnerRangeLength.setEnabled(false);
			} else if (rangeMethod.equals(MapViewProperties.getString("String_RangeMode_StdDeviation"))) {
				// 标准差分段
				rangeMode = RangeMode.STDDEVIATION;
				comboBoxRangeCount.setEnabled(false);
				spinnerRangeLength.setEnabled(false);
			} else if (rangeMethod.equals(MapViewProperties.getString("String_RangeMode_Logarithm"))) {
				// 对数分度
				rangeMode = rangeMode.LOGARITHM;
				comboBoxRangeCount.setEnabled(true);
				spinnerRangeLength.setEnabled(false);
			} else if (rangeMethod.equals(MapViewProperties.getString("String_RangeMode_Quantile"))) {
				// 等计数分段
				rangeMode = rangeMode.QUANTILE;
				comboBoxRangeCount.setEnabled(true);
				spinnerRangeLength.setEnabled(false);
			} else if (rangeMethod.equals(MapViewProperties.getString("String_RangeMode_CustomInterval"))) {
				// 自定义分段
				rangeMode = RangeMode.CUSTOMINTERVAL;
				comboBoxRangeCount.setEnabled(false);
				spinnerRangeLength.setEnabled(true);
			}
			resetThemeInfo();
		}

		/**
		 * 设置分段舍入精度
		 */
		private void setRangePrecision() {
			int index = comboBoxRangePrecision.getSelectedIndex();
			if (index == 0) {
				numeric = "#";
			} else if (index == 1) {
				numeric = "#.0";
			} else if (index == 2) {
				numeric = "#.00";
			} else if (index == 3) {
				numeric = "#.000";
			} else if (index == 4) {
				numeric = "#.0000";
			} else if (index == 5) {
				numeric = "#.00000";
			} else if (index == 6) {
				numeric = "#.000000";
			} else if (index == 7) {
				numeric = "#.0000000";
			}
			refreshTable();
		}

		/**
		 * 字段表达式
		 */
		private void setFieldInfo() {
			rangeExpression = (String) comboBoxExpression.getSelectedItem();
			rangeExpression = rangeExpression.substring(rangeExpression.lastIndexOf(".") + 1, rangeExpression.length());
			resetThemeInfo();
		}

		private void resetThemeInfo() {
			ThemeRange theme = ThemeRange.makeDefault(datasetVector, rangeExpression, rangeMode, rangeCount, ColorGradientType.GREENRED);
			if (null != theme) {
				map.getLayers().remove(themeRangeLayer);
				themeRangeLayer = map.getLayers().add(datasetVector, theme, true);
				UICommonToolkit.getLayersManager().getLayersTree().reload();
				themeRange = (ThemeRange) themeRangeLayer.getTheme();
				textFieldRangeValue.setText(new DecimalFormat(numeric).format(themeRange.getItem(0).getEnd()));
				comboBoxRangeCount.setSelectedIndex(rangeCount - 2);
				refreshColor();
				map.refresh();
				refreshTable();
			}
		}

		/**
		 * 获取表达式项
		 * 
		 * @param jComboBoxField
		 */
		private void getSqlExpression(JComboBox<String> jComboBoxField) {
			// 判断是否为“表达式”项
			if (MapViewProperties.getString("String_Combobox_Expression").equals(jComboBoxField.getSelectedItem())) {
				SQLExpressionDialog sqlDialog = new SQLExpressionDialog();
				int allItems = jComboBoxField.getItemCount();
				Dataset[] datasets = new Dataset[1];
				datasets[0] = datasetVector;
				DialogResult dialogResult = sqlDialog.showDialog(datasets);
				if (dialogResult == DialogResult.OK) {
					String filter = sqlDialog.getQueryParameter().getAttributeFilter();
					if (filter != null && !filter.trim().equals("")) {
						jComboBoxField.insertItemAt(filter, allItems - 1);
						jComboBoxField.setSelectedIndex(allItems - 1);
					}
				}

			}
		}

	}
}
