package com.supermap.desktop.newtheme;
import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.image.BufferedImage;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 * 包含Panel的类,用于table渲染颜色方案
 * 
 * @author xie
 * 
 */
public class JPanelRendererGeoStyle extends JPanel implements TableCellRenderer {
	private static final long serialVersionUID = 1L;

	private JLabel jLabel;


	public JPanelRendererGeoStyle() {
		super();
		this.setLayout(new GridLayout(1, 1));
		this.setBorder(BorderFactory.createEmptyBorder(0, 0, 2, 2));
		jLabel = new JLabel();
		this.add(jLabel);
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		ImageIcon icon = new ImageIcon();
		BufferedImage bi = new BufferedImage(30, 30, BufferedImage.TYPE_INT_ARGB);
		Graphics2D gg = (Graphics2D) bi.getGraphics();
		gg.setColor(((UniqueValue) table.getValueAt(row, column)).getColor());
		gg.fillRect(5, 8, 30, 30);
		icon.setImage(bi);
		jLabel.setIcon(icon);
		return this;
	}
}
