package com.supermap.desktop.newtheme;

import java.awt.FlowLayout;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JList;

import com.supermap.desktop.mapview.MapViewProperties;
import com.supermap.desktop.properties.CommonProperties;
import com.supermap.desktop.ui.controls.CommonListCellRenderer;
import com.supermap.desktop.ui.controls.DataCell;
import com.supermap.desktop.ui.controls.InternalImageIconFactory;
import com.supermap.desktop.ui.controls.SmDialog;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.border.LineBorder;

import java.awt.Window.Type;

public class ThemeGuidDialog extends SmDialog {
	private static final long serialVersionUID = 1L;
	private final JPanel panelContent = new JPanel();
	private JButton buttonOk = new JButton("OK");
	private JButton buttonCancel = new JButton("Cancel");
	private JList<Object> listContent = new JList<Object>();
	private LocalActionListener actionListener = new LocalActionListener();
	private GroupLayout gl_panelContent = new GroupLayout(panelContent);
	private LocalListSelectionListener listSelectionListener = new LocalListSelectionListener();

	public static void main(String[] args) {
		try {
			ThemeGuidDialog dialog = new ThemeGuidDialog(null, false);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ThemeGuidDialog(JFrame owner, boolean flag) {
		super(owner, true);
		initComponents();
		initResources();
		registListener();
	}

	private void initComponents() {
		setBounds(500, 400, 475, 385);
		this.panelContent.setBorder(new EmptyBorder(5, 5, 5, 5));

		JScrollPane scrollPane = new JScrollPane();

		UniqueThemePanel panel = new UniqueThemePanel();
		panel.setBorder(new LineBorder(Color.LIGHT_GRAY));
		panel.setBackground(Color.WHITE);
		gl_panelContent.setHorizontalGroup(
				gl_panelContent.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panelContent.createSequentialGroup()
								.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 147, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(panel, GroupLayout.DEFAULT_SIZE, 286, Short.MAX_VALUE)
								.addContainerGap())
				);
		gl_panelContent.setVerticalGroup(
				gl_panelContent.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panelContent.createSequentialGroup()
								.addGap(2)
								.addGroup(gl_panelContent.createParallelGroup(Alignment.LEADING)
										.addComponent(panel, GroupLayout.DEFAULT_SIZE, 302, Short.MAX_VALUE)
										.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 302, Short.MAX_VALUE)))
				);
		addListItem();
		scrollPane.setViewportView(listContent);
		this.panelContent.setLayout(gl_panelContent);
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonOk.setActionCommand("OK");
		buttonPane.add(buttonOk);
		getRootPane().setDefaultButton(buttonOk);
		buttonCancel.setActionCommand("Cancel");
		buttonPane.add(buttonCancel);
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(panelContent, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(buttonPane, GroupLayout.PREFERRED_SIZE, 459, GroupLayout.PREFERRED_SIZE)
				);
		groupLayout.setVerticalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
								.addComponent(panelContent, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(buttonPane, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
				);
		getContentPane().setLayout(groupLayout);
	}

	private void addListItem() {
		DefaultListModel<Object> listModel = new DefaultListModel<Object>();
		DataCell uniqueDataCell = new DataCell(InternalImageIconFactory.THEMEGUIDE_UNIQUE, MapViewProperties.getString("String_Theme_Unique"));
		DataCell rangeDataCell = new DataCell(InternalImageIconFactory.THEMEGUIDE_LABEL, MapViewProperties.getString("String_Theme_Range"));
		DataCell labelDataCell = new DataCell(InternalImageIconFactory.THEMEGUIDE_RANGE, MapViewProperties.getString("String_ThemeLabel"));
		listModel.addElement(uniqueDataCell);
		listModel.addElement(rangeDataCell);
		listModel.addElement(labelDataCell);
		this.listContent.setModel(listModel);
		this.listContent.setCellRenderer(new CommonListCellRenderer());
	}

	private void initResources() {
		this.setTitle(MapViewProperties.getString("String_ThemeGuide"));
		this.buttonOk.setText(CommonProperties.getString("String_Button_OK"));
		this.buttonCancel.setText(CommonProperties.getString("String_Button_Cancel"));
	}

	private void registListener() {
		this.buttonOk.addActionListener(actionListener);
		this.buttonCancel.addActionListener(actionListener);
		this.listContent.addListSelectionListener(listSelectionListener);
	}

	protected void replaceCurrentPanelByListSelection(int selectRow) {
		switch (selectRow) {
		case 0:
			if (null != getRightPanel()) {
				this.gl_panelContent.replace(getRightPanel(), new UniqueThemePanel());
			}
			break;
		case 1:
			this.gl_panelContent.replace(getRightPanel(), new RangeThemePanel());
			break;
		case 2:
			this.gl_panelContent.replace(getRightPanel(), new LabelThemePanel());
			break;

		default:
			break;
		}
	}

	private void unregistListener() {
		this.buttonOk.removeActionListener(actionListener);
		this.listContent.removeListSelectionListener(listSelectionListener);

	}

	private JPanel getRightPanel() {
		return (JPanel) this.panelContent.getComponent(1);
	}

	class LocalListSelectionListener implements ListSelectionListener {

		@Override
		public void valueChanged(ListSelectionEvent e) {
			int selectRow = ThemeGuidDialog.this.listContent.getSelectedIndex();
			replaceCurrentPanelByListSelection(selectRow);
		}

	}

	class LocalActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if ("OK".equals(e.getActionCommand())) {
				int clickCount = listContent.getSelectedIndex();
				if (0 == clickCount) {
					//单值专题图
					ThemeGuideFactory.buildUniqueTheme();
					ThemeGuidDialog.this.dispose();
				} else if (1 == clickCount) {
					//分段专题图
					ThemeGuideFactory.buildRangeTheme();
					ThemeGuidDialog.this.dispose();
				}else if (2 == clickCount) {
					//标签专题图
				}
			} else if ("Cancel".equals(e.getActionCommand())) {
				unregistListener();
				ThemeGuidDialog.this.buttonCancel.removeActionListener(actionListener);
				ThemeGuidDialog.this.dispose();
			}
		}

	}

}
