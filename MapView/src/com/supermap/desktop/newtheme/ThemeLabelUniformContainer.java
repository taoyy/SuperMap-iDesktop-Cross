package com.supermap.desktop.newtheme;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;

import com.supermap.desktop.controls.ControlsProperties;
import com.supermap.desktop.mapview.MapViewProperties;
import com.supermap.desktop.ui.controls.GridBagConstraintsHelper;

import javax.swing.border.TitledBorder;

public class ThemeLabelUniformContainer extends JPanel {

	private static final long serialVersionUID = 1L;

	private JTabbedPane tabbedPane = new JTabbedPane();
	private ThemeLabelPropertyPanel panelProperty = new ThemeLabelPropertyPanel();
	private JPanel panelSytle = new JPanel();
	private ThemeLabelAdvancePanel panelAdvance = new ThemeLabelAdvancePanel();
	// panelStyle
	private JLabel labelFontName = new JLabel();
	private JComboBox<String> comboBoxFontName = new JComboBox<String>();
	private JLabel labelAlign = new JLabel();
	private JComboBox<String> comboBoxAlign = new JComboBox<String>();
	private JLabel labelFontSize = new JLabel();
	private JComboBox<String> comboBoxFontSize = new JComboBox<String>();
	private JLabel labelFontHeight = new JLabel();
	private JSpinner spinnerFontHeight = new JSpinner();
	private JLabel labelFontWide = new JLabel();
	private JSpinner spinnerFontWide = new JSpinner();
	private JLabel labelRotationAngl = new JLabel();
	private JSpinner spinnerRotationAngl = new JSpinner();
	private JLabel labelInclinationAngl = new JLabel();
	private JSpinner spinnerInclinationAngl = new JSpinner();
	private JLabel labelFontColor = new JLabel();
	private JComboBox<String> comboBoxFontColor = new JComboBox<String>();
	private JLabel labelBGColor = new JLabel();
	private JComboBox<String> comboBoxBGColor = new JComboBox<String>();
	// panelFontEffect
	private JCheckBox checkBoxBorder = new JCheckBox();
	private JCheckBox checkBoxStrickout = new JCheckBox();
	private JCheckBox checkBoxItalic = new JCheckBox();
	private JCheckBox checkBoxUnderline = new JCheckBox();
	private JCheckBox checkBoxShadow = new JCheckBox();
	private JCheckBox checkBoxFixedSize = new JCheckBox();
	private JCheckBox checkBoxOutlook = new JCheckBox();
	private JCheckBox checkBoxBGTransparent = new JCheckBox();

	public ThemeLabelUniformContainer() {
		initComponents();
		initResources();
	}

	private void initComponents() {
		this.setLayout(new GridBagLayout());
		this.tabbedPane.add(MapViewProperties.getString("String_Theme_Property"), this.panelProperty);
		this.tabbedPane.add(MapViewProperties.getString("String_Theme_Style"), this.panelSytle);
		this.tabbedPane.add(MapViewProperties.getString("String_Theme_Advanced"), this.panelAdvance);
		this.add(this.tabbedPane, new GridBagConstraintsHelper(0, 0, 1, 1).setAnchor(GridBagConstraints.CENTER).setFill(GridBagConstraints.BOTH)
				.setWeight(1, 1));
		initPanelStyle();
	}

	private void initResources() {

		this.labelFontName.setText(ControlsProperties.getString("String_GeometryPropertyTextControl_LabelFontName"));
		this.labelAlign.setText(ControlsProperties.getString("String_GeometryPropertyTextControl_LabelAlinement"));
		this.labelFontSize.setText(ControlsProperties.getString("String_GeometryPropertyTextControl_LabelFontSize"));
		this.labelFontHeight.setText(ControlsProperties.getString("String_GeometryPropertyTextControl_LabelFontHeight"));
		this.labelFontWide.setText(ControlsProperties.getString("String_GeometryPropertyTextControl_LabelFontWidth"));
		this.labelRotationAngl.setText(ControlsProperties.getString("String_Label_SymbolAngle"));
		this.labelInclinationAngl.setText(ControlsProperties.getString("String_Label_ItalicAngle"));
		this.labelFontColor.setText(ControlsProperties.getString("String_Label_TextStyleForeColor"));
		this.labelBGColor.setText(MapViewProperties.getString("String_Label_BackColor"));

		this.checkBoxBorder.setText(ControlsProperties.getString("String_OverStriking"));
		this.checkBoxStrickout.setText(ControlsProperties.getString("String_DeleteLine"));
		this.checkBoxItalic.setText(ControlsProperties.getString("String_Italic"));
		this.checkBoxUnderline.setText(ControlsProperties.getString("String_Underline"));
		this.checkBoxShadow.setText(ControlsProperties.getString("String_Shadow"));
		this.checkBoxFixedSize.setText(ControlsProperties.getString("String_FixedSize"));
		this.checkBoxOutlook.setText(ControlsProperties.getString("String_Contour"));
		this.checkBoxBGTransparent.setText(ControlsProperties.getString("String_BackgroundTransparency"));

	}





	private void initPanelStyle() {
		//@formatter:off
		this.panelSytle.setLayout(new GridBagLayout());
		this.panelSytle.add(this.labelFontName,          new GridBagConstraintsHelper(0, 0, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		comboBoxFontName.setEditable(true);
		this.panelSytle.add(this.comboBoxFontName,       new GridBagConstraintsHelper(1, 0, 1, 1).setAnchor(GridBagConstraints.CENTER).setWeight(1, 0).setInsets(2,10,2,10).setIpad(30, 0));
		this.panelSytle.add(this.labelAlign,             new GridBagConstraintsHelper(0, 1, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		comboBoxAlign.setEditable(true);
		this.panelSytle.add(this.comboBoxAlign,          new GridBagConstraintsHelper(1, 1, 1, 1).setAnchor(GridBagConstraints.CENTER).setWeight(1, 0).setInsets(2,10,2,10).setIpad(30, 0));
		this.panelSytle.add(this.labelFontSize,          new GridBagConstraintsHelper(0, 2, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		comboBoxFontSize.setEditable(true);
		this.panelSytle.add(this.comboBoxFontSize,       new GridBagConstraintsHelper(1, 2, 1, 1).setAnchor(GridBagConstraints.CENTER).setWeight(1, 0).setInsets(2,10,2,10).setIpad(30, 0));
		this.panelSytle.add(this.labelFontHeight,        new GridBagConstraintsHelper(0, 3, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		this.panelSytle.add(this.spinnerFontHeight,      new GridBagConstraintsHelper(1, 3, 1, 1).setAnchor(GridBagConstraints.CENTER).setWeight(1, 0).setInsets(2,10,2,10).setIpad(30, 0));
		this.panelSytle.add(this.labelFontWide,          new GridBagConstraintsHelper(0, 4, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		this.panelSytle.add(this.spinnerFontWide,        new GridBagConstraintsHelper(1, 4, 1, 1).setAnchor(GridBagConstraints.CENTER).setWeight(1, 0).setInsets(2,10,2,10).setIpad(30, 0));
		this.panelSytle.add(this.labelRotationAngl,      new GridBagConstraintsHelper(0, 5, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		this.panelSytle.add(this.spinnerRotationAngl,    new GridBagConstraintsHelper(1, 5, 1, 1).setAnchor(GridBagConstraints.CENTER).setWeight(1, 0).setInsets(2,10,2,10).setIpad(30, 0));
		this.panelSytle.add(this.labelInclinationAngl,   new GridBagConstraintsHelper(0, 6, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		this.panelSytle.add(this.spinnerInclinationAngl, new GridBagConstraintsHelper(1, 6, 1, 1).setAnchor(GridBagConstraints.CENTER).setWeight(1, 0).setInsets(2,10,2,10).setIpad(30, 0));
		this.panelSytle.add(this.labelFontColor,         new GridBagConstraintsHelper(0, 7, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		comboBoxFontColor.setEditable(true);
		this.panelSytle.add(this.comboBoxFontColor,      new GridBagConstraintsHelper(1, 7, 1, 1).setAnchor(GridBagConstraints.CENTER).setWeight(1, 0).setInsets(2,10,2,10).setIpad(30, 0));
		this.panelSytle.add(this.labelBGColor,           new GridBagConstraintsHelper(0, 8, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		comboBoxBGColor.setEditable(true);
		this.panelSytle.add(this.comboBoxBGColor,        new GridBagConstraintsHelper(1, 8, 1, 1).setAnchor(GridBagConstraints.CENTER).setWeight(1, 0).setInsets(2,10,2,10).setIpad(30, 0));
		JPanel panelFontEffect = new JPanel();
		panelFontEffect.setBorder(new TitledBorder(null, ControlsProperties.getString("String_GeometryPropertyTextControl_GroupBoxFontEffect"), TitledBorder.LEADING, TitledBorder.TOP, null, null));
		initPanelFontEffect(panelFontEffect);
		this.panelSytle.add(panelFontEffect,             new GridBagConstraintsHelper(0, 9, 2, 1).setAnchor(GridBagConstraints.CENTER).setWeight(1, 0).setInsets(5).setFill(GridBagConstraints.HORIZONTAL));
		//@formatter:on
	}

	private void initPanelFontEffect(JPanel panelFontEffect) {
		//@formatter:off
		panelFontEffect.setLayout(new GridBagLayout());
		panelFontEffect.add(this.checkBoxBorder,         new GridBagConstraintsHelper(0, 0, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		panelFontEffect.add(this.checkBoxStrickout,      new GridBagConstraintsHelper(1, 0, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		panelFontEffect.add(this.checkBoxItalic,         new GridBagConstraintsHelper(0, 1, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		panelFontEffect.add(this.checkBoxUnderline,      new GridBagConstraintsHelper(1, 1, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		panelFontEffect.add(this.checkBoxShadow,         new GridBagConstraintsHelper(0, 2, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		panelFontEffect.add(this.checkBoxFixedSize,      new GridBagConstraintsHelper(1, 2, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		panelFontEffect.add(this.checkBoxOutlook,        new GridBagConstraintsHelper(0, 3, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		panelFontEffect.add(this.checkBoxBGTransparent,  new GridBagConstraintsHelper(1, 3, 1, 1).setAnchor(GridBagConstraints.WEST).setWeight(1, 0).setInsets(2,10,2,10));
		//@formatter:on
	}



}
