package com.supermap.desktop.newtheme;

import javax.swing.JPanel;

import java.awt.Color;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.border.LineBorder;

import com.supermap.desktop.mapview.MapViewProperties;
import com.supermap.desktop.ui.controls.InternalImageIconFactory;

public class RangeThemePanel extends JPanel {

	private static final long serialVersionUID = 1L;
	JLabel labelRangeTheme = new JLabel("");
	public RangeThemePanel() {
		initComponents();
		initResources();
	}
	private void initComponents(){
		setBorder(new LineBorder(Color.LIGHT_GRAY));
		setBackground(Color.WHITE);
		
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(20)
					.addComponent(labelRangeTheme)
					.addContainerGap(368, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(24)
					.addComponent(labelRangeTheme)
					.addContainerGap(223, Short.MAX_VALUE))
		);
		setLayout(groupLayout);
	}
	private void initResources(){
		labelRangeTheme.setIcon(InternalImageIconFactory.THEMEGUIDE_RANGE);
		labelRangeTheme.setText(MapViewProperties.getString("String_Theme_Range"));
		labelRangeTheme.setVerticalTextPosition(JLabel.BOTTOM);
		labelRangeTheme.setHorizontalTextPosition(JLabel.CENTER);
	}
	
	private void registListener(){
		
	}
	
	private void unregistListener(){
		
	}
	
}
