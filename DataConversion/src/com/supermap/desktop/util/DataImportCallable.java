package com.supermap.desktop.util;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CancellationException;

import javax.swing.JTable;

import com.supermap.data.Dataset;
import com.supermap.data.DatasetVector;
import com.supermap.data.Datasource;
import com.supermap.data.SpatialIndexInfo;
import com.supermap.data.conversion.DataImport;
import com.supermap.data.conversion.ImportResult;
import com.supermap.data.conversion.ImportSetting;
import com.supermap.data.conversion.ImportSettingWOR;
import com.supermap.data.conversion.ImportSettings;
import com.supermap.data.conversion.ImportSteppedEvent;
import com.supermap.data.conversion.ImportSteppedListener;
import com.supermap.desktop.Application;
import com.supermap.desktop.ImportFileInfo;
import com.supermap.desktop.dataconversion.DataConversionProperties;
import com.supermap.desktop.progress.Interface.UpdateProgressCallable;
import com.supermap.desktop.ui.UICommonToolkit;

public class DataImportCallable extends UpdateProgressCallable {
	private ArrayList<ImportFileInfo> fileInfos;
	private JTable table;

	public DataImportCallable(List<ImportFileInfo> fileInfos, JTable table) {
		this.fileInfos = (ArrayList<ImportFileInfo>) fileInfos;
		this.table = table;
	}

	@Override
	public Boolean call() {
		try {
			for (int i = 0; i < fileInfos.size(); i++) {
				final DataImport dataImport = new DataImport();
				ImportSettings importSettings = dataImport.getImportSettings();
				ImportFileInfo fileInfo = fileInfos.get(i);
				ImportSetting importSetting = fileInfo.getImportSetting();
				importSetting.setSourceFilePath(fileInfo.getFilePath());
				Datasource tempDatasource = fileInfo.getTargetDatasource();
				if (null != tempDatasource) {
					importSetting.setTargetDatasource(tempDatasource);
				} else {
					Datasource datasource = Application.getActiveApplication().getActiveDatasources()[0];
					importSetting.setTargetDatasource(datasource);
				}
				if (importSetting instanceof ImportSettingWOR) {
					((ImportSettingWOR) importSetting).setTargetWorkspace(Application.getActiveApplication().getWorkspace());
				}
				importSettings.add(importSetting);
				PercentProgress percentProgress = new PercentProgress(i);
				dataImport.addImportSteppedListener(percentProgress);
				long startTime = System.currentTimeMillis(); // 获取开始时间

				ImportResult result = dataImport.run();

				long endTime = System.currentTimeMillis(); // 获取结束时间
				String time = String.valueOf((endTime - startTime) / 1000.0);
				printMessage(result, i, time);
				// 更新行
				((FileInfoModel) table.getModel()).updateRows(fileInfos);
				if (null != percentProgress && percentProgress.isCancel()) {
					break;
				}
			}

		} catch (Exception e2) {
			Application.getActiveApplication().getOutput().output(e2);
		}
		return true;
	}

	/**
	 * 进度事件得到运行时间
	 * 
	 * @author Administrator
	 *
	 */
	class PercentProgress implements ImportSteppedListener {
		private int i;
		private boolean isCancel = false;

		public PercentProgress(int i) {
			this.i = i;
		}

		public boolean isCancel() {
			return this.isCancel;
		}

		@Override
		public void stepped(ImportSteppedEvent arg0) {
			try {
				double count = fileInfos.size();
				int totalPercent = (int) (((i + 0.0) / count) * 100);
				updateProgressTotal(arg0.getSubPercent(), totalPercent, MessageFormat.format(
						DataConversionProperties.getString("String_ProgressControl_TotalImportProgress"), String.valueOf(totalPercent),
						String.valueOf(fileInfos.size())), MessageFormat.format(DataConversionProperties.getString("String_FileInput"), arg0.getCurrentTask()
						.getSourceFilePath()));
			} catch (CancellationException e) {
				arg0.setCancel(true);
				this.isCancel = true;
			}
		}
	}

	/**
	 * 打印导入信息
	 * 
	 * @param result
	 * @param i
	 */
	private void printMessage(ImportResult result, int i, String time) {
		ImportSetting[] successImportSettings = result.getSucceedSettings();
		ImportSetting[] failImportSettings = result.getFailedSettings();
		String successImportInfo = DataConversionProperties.getString("String_FormImport_OutPutInfoOne");
		String failImportInfo = DataConversionProperties.getString("String_FormImport_OutPutInfoTwo");
		if (null != successImportSettings && 0 < successImportSettings.length) {
			// 创建空间索引，字段索引
			fileInfos.get(i).setState(DataConversionProperties.getString("String_FormImport_Succeed"));
			// 导入成功提示信息
			ImportSetting sucessSetting = successImportSettings[0];
			String datasetName = sucessSetting.getTargetDatasetName();
			Dataset dataset = sucessSetting.getTargetDatasource().getDatasets().get(datasetName);
			boolean isBuildSpatialIndex = fileInfos.get(i).isBuildSpatialIndex();
			boolean isBuildFiledIndex = fileInfos.get(i).isBuildFiledIndex();
			if (dataset instanceof DatasetVector) {
				if (isBuildFiledIndex) {
					int count = ((DatasetVector) dataset).getFieldInfos().getCount();
					for (int j = 0; j < count; j++) {
						String fieldName = ((DatasetVector) dataset).getFieldInfos().get(j).getName();
						String indexName = MessageFormat.format("{0}_{1}", fieldName, UUID.randomUUID());
						if (indexName.length() > 30)
						{
							indexName = indexName.substring(0, 30);
						}
						indexName = indexName.replace(DataConversionProperties.getString("String_Horizatal"), "");
						((DatasetVector) dataset).buildFieldIndex(new String[] { fieldName }, indexName);
					}

				} else if (isBuildSpatialIndex) {
					((DatasetVector) dataset).buildSpatialIndex(new SpatialIndexInfo());
				}
			}
			Application
					.getActiveApplication()
					.getOutput()
					.output(MessageFormat.format(successImportInfo, sucessSetting.getSourceFilePath(), "->", sucessSetting.getTargetDatasetName(),
							sucessSetting.getTargetDatasource().getAlias(), time));
			UICommonToolkit.refreshSelectedDatasourceNode(sucessSetting.getTargetDatasource().getAlias());
		} else if (null != failImportSettings && 0 < failImportSettings.length) {
			fileInfos.get(i).setState(DataConversionProperties.getString("String_FormImport_NotSucceed"));
			Application.getActiveApplication().getOutput().output(MessageFormat.format(failImportInfo, failImportSettings[0].getSourceFilePath(), ">", ""));
		}
	}

}
