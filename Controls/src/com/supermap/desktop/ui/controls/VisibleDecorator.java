package com.supermap.desktop.ui.controls;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import com.supermap.mapping.Layer;
import com.supermap.mapping.ThemeGridRangeItem;
import com.supermap.mapping.ThemeGridUniqueItem;
import com.supermap.mapping.ThemeLabelItem;
import com.supermap.mapping.ThemeRangeItem;
import com.supermap.mapping.ThemeUniqueItem;
import com.supermap.realspace.Feature3D;
import com.supermap.realspace.Feature3Ds;
import com.supermap.realspace.Layer3D;
import com.supermap.realspace.ScreenLayer3D;
import com.supermap.realspace.TerrainLayer;
import com.supermap.realspace.Theme3DRangeItem;
import com.supermap.realspace.Theme3DUniqueItem;

/**
 * 图层是否可见节点装饰器
 * @author hmily
 *
 */
class VisibleDecorator implements TreeNodeDecorator {

	public void decorate(JLabel label, TreeNodeData data) {
//		if(DecoratorUnities.isDecoratorShow(data.getData())){
//			return;
//		}
		boolean isVisible = false;
		ImageIcon icon = (ImageIcon) label.getIcon();
		BufferedImage bufferedImage = new BufferedImage(
				IMAGEICON_WIDTH, IMAGEICON_HEIGHT,
				BufferedImage.TYPE_INT_ARGB);
		Graphics graphics = bufferedImage.getGraphics();
		Object obj = data.getData();
		isVisible = getState(obj);
		if (isVisible) {
			graphics.drawImage(InternalImageIconFactory.VISIBLE
					.getImage(), 0, 0, label);
		} else {
			graphics.drawImage(InternalImageIconFactory.INVISIBLE
					.getImage(), 0, 0, label);
		}
		icon.setImage(bufferedImage);
	}
	
	
	public boolean getState(Object internalData){
		boolean isVisible = false;
		if (internalData instanceof ThemeGridRangeItem) {
			ThemeGridRangeItem gridRangeItem = (ThemeGridRangeItem) internalData;
			isVisible = gridRangeItem.isVisible();
		} else if (internalData instanceof ThemeGridUniqueItem) {
			ThemeGridUniqueItem gridUniqueItem = (ThemeGridUniqueItem) internalData;
			isVisible = gridUniqueItem.isVisible();
		} else if (internalData instanceof ThemeLabelItem) {
			ThemeLabelItem labelItem = (ThemeLabelItem) internalData;
			isVisible = labelItem.isVisible();
		} else if (internalData instanceof ThemeRangeItem) {
			ThemeRangeItem rangeItem = (ThemeRangeItem) internalData;
			isVisible = rangeItem.isVisible();
		} else if (internalData instanceof ThemeUniqueItem) {
			ThemeUniqueItem uniqueItem = (ThemeUniqueItem) internalData;
			isVisible = uniqueItem.isVisible();
		}else if(internalData instanceof Layer){
			Layer layer = (Layer) internalData;
			isVisible = layer.isVisible();
		}else if(internalData instanceof Layer3D){
			Layer3D layer3D = (Layer3D) internalData;
			isVisible = layer3D.isVisible();
		}else if(internalData instanceof ScreenLayer3D){
			ScreenLayer3D screenLayer3D = (ScreenLayer3D) internalData;
			isVisible = screenLayer3D.isVisible();
		}else if(internalData instanceof Theme3DRangeItem){
			Theme3DRangeItem theme3DRangeItem = (Theme3DRangeItem) internalData;
			isVisible = theme3DRangeItem.isVisible();
		}else if(internalData instanceof Theme3DUniqueItem){
			Theme3DUniqueItem item = (Theme3DUniqueItem) internalData;
			isVisible = item.isVisible();
		}else if(internalData instanceof TerrainLayer){
			TerrainLayer terrainLayer = (TerrainLayer) internalData;
			isVisible = terrainLayer.isVisible();
		}else if(internalData instanceof Feature3D){
			Feature3D feature3D = (Feature3D) internalData;
			isVisible = feature3D.isVisible();
		}else if(internalData instanceof Feature3Ds){
			Feature3Ds feature3Ds = (Feature3Ds) internalData;
			isVisible = feature3Ds.isVisible();
		}
		return isVisible;
	}
}
